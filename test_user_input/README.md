


From: https://qunitjs.com/cookbook/

syn "is a synthetic event library that pretty much handles typing, clicking,
moving, and dragging exactly how a real user would perform those actions".
Used by FuncUnit, which is based on QUnit, for functional testing of web
applications.

JSRobot - "A testing utility for web-apps that can generate real keystrokes
rather than simply simulating the JavaScript event firing. This allows the
keystroke to trigger the built-in browser behaviour which isn't otherwise
possible."

DOH Robot "provides an API that enables testers to automate their UI tests
using real, cross-platform, system-level input events". This gets you the
closest to "real" browser events, but uses Java applets for that.

keyvent.js - "Keyboard events simulator."
