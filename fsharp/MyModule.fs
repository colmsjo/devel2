module MyModule

type MyCell(n:int) =
  let mutable data = n + 1
  do printf "Creating MyCell(%d)" n

  member x.Data
    with get() = data
    and set(v) = data <- v

  member x.Print() =
    printf "Data: %d" data
