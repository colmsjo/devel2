Introduction
-----------

Misc. notes about the F# language. F# is a multi-paradigm language that is
very similar the functional language ML. It also support traditional imperative
style as well as objects. The functional parts makes it a very powerfull tool.

JavaScript also supports a functional programming style. Both F# and JavaScript
support so called lambda calculus (or anonymous functions) and closures. F#
has some nice features that JavaScript lacks, for instance tuples and also
so called dicriminated unions and pattern matching.


Nice data types and comprehensions
----------------------------------

F# has tuples, list and list comprehensions:

    let people = [ ("Joe", 55); ("John", 32); ("Jane", 24) ]

    [ for (name,age) in people do
      if age < 30 then
        yield name ]


  open System

  type Variant =
    | Some of int
    | None

  let readNum () =
    let s = Console.ReadLine()
    let ok,v = Int32.TryParse(s)
    if (ok) then Some(v) else None

It is possible to see a lot information of a type like this:

  typeof<Variant>;;



Computation expressions:


    type M<'a> = option<'a>

    let bind d f =
      match d with
        | None -> None
        | Some(v) -> f v
    let result v = Some(v)
    let delay f = f()

    type MaybeBuilder() =
      member x.Bind(v, f) = bind v f
      member x.Return(v) = result v
      member x.Delay(f) = delay f

    let maybe = MaybeBuilder()

    let n =
      maybe { do printf "Enter a: "
              let! a = readNum()
              do printf "Enter b: "
              let! b = readNum()
              return a + b }
      printf "Result is: %A" n



Custom modules
-------------

1. Compile module with: `fsharpc -a MyModule.fs`
2. Use the module:

    12:08:56-jonas2~/git/colmsjo/devel2/fsharp (master)$ sharpi
    -bash: sharpi: command not found
    12:09:02-jonas2~/git/colmsjo/devel2/fsharp (master)$ fsharpi

    F# Interactive for F# 3.1 (Open Source Edition)
    Freely distributed under the Apache 2.0 Open Source License

    For help type #help;;

    > #r "MyModule.dll"
    - ;;

    --> Referenced '/Users/jonas2/git/colmsjo/devel2/fsharp/MyModule.dll' (file may be locked by F# Interactive process)

    > let o = new MyModule.MyCell(3);;
    Creating MyCell(3)
    val o : MyModule.MyCell


Scripts
--------

The `MyModule.fs` file can also be loaded as a script (without compiling it first):

    > #load "MyModule.fs";;
    [Loading /Users/jonas2/git/colmsjo/devel2/fsharp/MyModule.fs]

    namespace FSI_0013
    type MyCell =
    class
    new : n:int -> MyCell
    member Print : unit -> unit
    member Data : int
    member Data : int with set
    end

    >
    > let o = new MyModule.MyCell(10);;
    Creating MyCell(10)
    val o : MyModule.MyCell

    > o.Data;;
    val it : int = 11
    > o.Data + 11;;
    val it : int = 22
    > o.Print();;
    Data: 11val it : unit = ()

Working with JSON
----------------

1. Install JSON library: `nuget install Fsharp.Data -OutputDirectory packages`
2. Start interactive sesion and use the JSON parser:
    09:21:31-jonas2~/git/colmsjo/devel2/fsharp (master)$ fsharpi

    F# Interactive for F# 3.1 (Open Source Edition)
    Freely distributed under the Apache 2.0 Open Source License

    For help type #help;;

    > #r "packages/FSharp.Data.2.1.1/lib/net40/FSharp.Data.dll"
    - open FSharp.Data;;

    --> Referenced '/Users/jonas2/git/colmsjo/devel2/fsharp/packages/FSharp.Data.2.1.1/lib/net40/FSharp.Data.dll' (file may be locked by F# Interactive process)

    > let info =
    -   JsonValue.Parse("""
    -     { "name": "Tomas", "born": 1985,
    -       "siblings": [ "Jan", "Alexander" ] } """);;

    val info : JsonValue =
      {
      "name": "Tomas",
      "born": 1985,
      "siblings": [
        "Jan",
        "Alexander"
      ]
    }

    > #quit;;

    - Exit...


Add members to types
-------------------

Have a look at this simple type:

  type Variant =
    | Num of int
    | Str of string with
    member x.Print() =
      match x with
      | Num(n) -> printf "Num %d" n
      | Str(s) -> printf "Str %s" s

A alternative way to write this type is to add members after the type has been
defined:

  type Variant =
    | Num of int
    | Str of string

  let print x =
    match x with
    | Num(n) -> printf "Num %d" n
    | Str(s) -> printf "Str %s" s

  type Variant with
    member x.Print() = print x


The benefit with this approach is that members can be added to a type in
different places of the code.



Reflection and quotations
-------------------------


Reflection and quotations are used to inspect language constructs like types and
objects at runtime.

  [<ReflectedDefinition>]
  let addOne x = x + 1;;

  let expandedQuotation =
    match ( <@ addOne @> ).Raw with
    | AnyTopDefnUse(td) ->
      match ResolveTopDefinition(td) with
        | Some(quot) -> quot
        | _ -> failwith "Quotation not available!"
    | _ -> failwith "Not a top level definition use!"

This is for instance used when compiling an EXE:

  [<EntryPoint>]
  let main argv =
    List.map printfn argv


Async
----

F# has support for async I/O. Don't mix this with traditional threading though:

    module Test =
      let work i =
        async {
          System.Threading.Thread.Sleep(500)
          return i+1
        }

      let run _ = [1..1000] |> List.map work |> Async.Parallel |> Async.RunSynchronously

    Test.run();;


This takes far longer than we would have thought. See more at: http://www.voyce.com/index.php/2011/05/27/fsharp-async-plays-well-with-others/#sthash.KygKeEx9.dpuf
