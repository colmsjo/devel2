//
// Install: `npm install promise`. Using https://www.npmjs.com/package/promise
//
// Code drom: https://www.promisejs.org
//

var fs = require('fs');
var Promise = require('promise');

// Return a promsie
function readFile2(filename){
  return new Promise(function (fulfill, reject){
    fs.readFile(filename, 'utf8', function (err, res){
      if (err) reject(err);
      else fulfill(res);
    });
  });
}

// now `readFile` will return a promise rather than expecting a callback
var readFile = Promise.denodeify(require('fs').readFile);

function readJSON(filename) {
  return readFile(filename, 'utf8').then(JSON.parse);
}

// If a callback is provided, call it with error as the first argument
// and result as the second argument, then return `undefined`.
// If no callback is provided, just return the promise.
function readJSON2(filename, callback) {
  return readFile(filename, 'utf8').then(JSON.parse).nodeify(callback);
}

console.log('- wellformed -');
readJSON2('wellformed.json')
  .then(function(res) {
    console.log('content: ' + JSON.stringify(res));
    console.log('- illformed -');
  })
  .then(readJSON('illformed.json'))
  .then(function(res) {
    console.log('content: ' + JSON.stringify(res));
  })
  .catch(function(err) {
    console.log('an error occured: ' + err)
  });
