var Promise = require('promise');

var when = function(p,f) {
  if(p === undefined || !f) throw 'both arguments are mandatory!'

  // instanceof don't work in browserify
  if(p.then) {
    return p.then(f);
  } else {
    return f(p);
  }
}

var deferred = function() {
  var args = Array.prototype.slice.call(arguments);
  var f = args.shift();
  if (typeof f !== 'function') return new Promise(function(fulfill, reject) {fulfill(f)});
  return new Promise(function(fulfill, reject) {fulfill( f.apply(this, args) )});
};

var promiseIf = function(b,p) {
  if(b === undefined || !p) throw 'both arguments are mandatory!'

  if(b) {
    return p;
  } else {
    return deferred(null);
  }
}

var promiseAnd = function (b, p1, p2) {
  if (!b) return deferred(null);
  return p1.then(function(res) {
    return promiseIf(res, p2);
  });
};

var promiseOr = function (b, p1, p2) {
  return p1.then(function(res) {
    return promiseIf(b || res, p2);
  });

};

when(deferred(function(msg) {console.log('should ' + msg); return true}, 'show a row with true'), console.log); 

promiseIf(false, deferred(true) ).then(function(x) {
    console.log('should be null: ' + x);
});

promiseIf(true, deferred(true) ).then(function(x) {
    console.log('should be true: ' + x);
});

promiseAnd(true, deferred(true), deferred(false)).then(function(x) {
  console.log('should be false: ' + x);
});

promiseAnd(true, deferred(true), deferred('Yea!')).then(function(x) {
  console.log('should be Yea!: ' + x);
});

promiseAnd(false, deferred(true), deferred('Yea!')).then(function(x) {
  console.log('should be null: ' + x);
});

promiseOr(false, deferred(true), deferred('Yea!')).then(function(x) {
  console.log('should be Yea!: ' + x);
});

promiseOr(false, deferred(false), deferred('Yea!')).then(function(x) {
  console.log('should be null: ' + x);
});
