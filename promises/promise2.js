//
// Install: `npm install promise`. Using https://www.npmjs.com/package/promise
//
// Code drom: https://www.promisejs.org
//

var fs = require('fs');
var Promise = require('promise');

// Return a promsie
function readFile(filename){
  return new Promise(function (fulfill, reject){
    fs.readFile(filename, 'utf8', function (err, res){
      if (err) reject(err);
      else fulfill(res);
    });
  });
}

function readJSON_(filename) {
  return readFile(filename, 'utf8').then(JSON.parse);
}

// Return promise. Can also be called with a callback the old fashion way
var readJSON = Promise.nodeify(readJSON_);


console.log('- wellformed -');
readJSON('wellformed.json')
  .then(function(res) {
    console.log('content: ' + JSON.stringify(res));
    console.log('- illformed -');
  })
  .then(function(res) {
    return readJSON('illformed.json')
  })
  .then(function(res) {
    console.log('content: ' + JSON.stringify(res));
  })
  .catch(function(err) {
    console.log('an error occured: ' + err);
  })
  .then(function() {
    console.log('- using callback -');
    readJSON('wellformed.json', function(err, res) {
        if(err) console.log('an error occured: ' + err);
        else console.log('content: ' + JSON.stringify(res));
    })

  })
