//
// Install: `npm install promise`. Using https://www.npmjs.com/package/promise
//
// Code drom: https://www.promisejs.org
//

var fs = require('fs');
var Promise = require('promise');

// Return a promsie
function readFile(filename) {
  return new Promise(function(fulfill, reject) {
    fs.readFile(filename, 'utf8', function(err, res) {
      if (err) reject(err);
      else fulfill(res);
    });
  });
}

function readJSON(filename) {
  return readFile(filename, 'utf8').then(JSON.parse);
}

function f() {
  console.log('- wellformed -');
  return readJSON('wellformed.json')
    .then(function(res) {
      console.log('content: ' + JSON.stringify(res));
      console.log('- illformed -');
    })
    .then(function(res) {
      return readJSON('illformed.json')
    })
    .then(function(res) {
      console.log('content: ' + JSON.stringify(res));
    })
    .catch(function(err) {
      console.log('an error occured: ' + err);
    })
}

f().then(function() {
  console.log('AFTER f()')
}).then(function() {
  console.log('YET ONE MORE THEN');
}).catch(function() {
  console.log('CAUGH AN ERROR');
}).then(function(){
  console.log('AFTER CATCH CLAUSE');
})
