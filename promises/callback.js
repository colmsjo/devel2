var fs = require('fs');

function readJSON(filename, callback){
  fs.readFile(filename, 'utf8', function (err, res){
    if (err) return callback(err);
    try {
      res = JSON.parse(res);
    } catch (ex) {
      return callback(ex);
    }
    callback(null, res);
  });
}

readJSON('wellformed.json', function(err, res) {
  console.log('- wellformed -');
  if(err) console.log('an error occured: '+err)
  else console.log('content: ' + JSON.stringify(res));
});

readJSON('illformed.json', function(err, res) {
  console.log('- illformed -');
  if(err) console.log('an error occured: '+err)
  else console.log('content: ' + JSON.stringify(res));
});
