var Promise = require('promise');

var promiseValue = function(p) {
  if(p instanceof Promise) {
    return p.then(function(q){
      return promiseValue(q);
    });
  } else {
    return p;
  }
}

var a = new Promise(function(fulfill, reject) {fulfill('a string');});
var b = new Promise(function(fulfill, reject) {fulfill(a);});
console.log('This will not work. A promise is returned. promiseValue(b) - ' + JSON.stringify(promiseValue(b)) ); 
promiseValue(b).then(function(res) {console.log('one more try - ' + JSON.stringify(promiseValue(res)));} ); 

var c = new Promise(function(fulfill, reject) {fulfill('one more string');});
var d = new Promise(function(fulfill, reject) {fulfill(c);});

var evalWithPromise = function(p, fn) {
  if(p instanceof Promise) {
    p.then(function(q){
      evalWithPromise(q, fn);
    });
  } else {
    fn(p);
  }
}

console.log('evalWithPromise(d, console.log)');
evalWithPromise(d, console.log);


