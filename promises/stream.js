//
// Install: `npm install promise`. Using https://www.npmjs.com/package/promise
//

var Promise = require('promise');

// create object that behaves lieke a stream that writes json into mysql
// actaully, it's note a stream, did not use `new require('stream').Readable;`
var jsonReadStream = {};
jsonReadStream.pipe = function(dest) {

  return new Promise(function(fulfill, reject) {
    dest.write(JSON.stringify({
      col1: 11,
      col2: '11'
    }));

    dest.write(JSON.stringify({
        col1: 22,
        col2: '22'
      }), 'utf-8',function() {
        fulfill();
      });
  });

};

jsonReadStream.pipe(process.stdout).then(function() {
  console.log('\nshould have printed JSON now!')
});
