// npm install promise

var Promise = require('promise');

var when = function(p,f) {
  if(p === undefined || !f) throw 'both arguments are mandatory!'

//  do not work in browserify - if(p instanceof Promise) {
  if(p.then) {
    return p.then(f);
  } else {
    return f(p);
  }
}

var deferred = function() {
//  return new Promise(function(fulfill, reject) {fulfill(f())});

  var args = Array.prototype.slice.call(arguments);
  var f = args.shift();

  return new Promise(function(fulfill, reject) {fulfill( f.apply(this, args) )});
};

when(false, console.log );
when(deferred(function(msg) {console.log('deferred log: ' + msg); return true}, 'hi there'), console.log); 
