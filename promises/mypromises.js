// Install: `npm install promise`. Using https://www.npmjs.com/package/promise
//
// Illustrate how many promises can be resolved
//

var Promise = require('promise');

console.log("Run Promise.all([Promise.resolve('a'), 'b', Promise.resolve('c')])");

Promise.all([Promise.resolve('a'), 'b', Promise.resolve('c')])
  .then(function (res) {
    console.log('In then, res: ' + res[0] + ':' + res[1] + ':' + res[2])

  });
