//
// Run with: `casperjs sample.js`
//


var casper = require('casper').create();

casper.start('http://127.0.0.1:8125/', function() {
    this.echo(this.getTitle());
});

casper.thenOpen('http://phantomjs.org', function() {
    this.echo(this.getTitle());
});

casper.thenOpen('http://di.se', function() {
    this.echo(this.getTitle());
});

casper.run();
