Install
------

I tested to install with `npm` and `brew` but neither worked.

```
$ git clone git://github.com/n1k0/casperjs.git
$ cd casperjs
$ ln -sf `pwd`/bin/casperjs /usr/local/bin/casperjs
```

Requires:

 * PhantomJS 1.8.2 (or greater)
 * Python 2.6 (or greater)


Run
----

Run the sample: `casperjs sample.js`

Run sample showing the test framework: `casperjs test hello-test.js`


Troubleshooting
--------------

1.
 * Q: `Couldn't find nor compute phantom.casperPath, exiting.`
 * A: See: https://github.com/n1k0/casperjs/issues/1150
