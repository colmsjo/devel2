//
// Install: `npm install minimist mysql`
// Run: `node nestedconnections.js --user=username --password=secret`
//

var argv = require('minimist')(process.argv.slice(2));

var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : argv['user'],
  password : argv['password']
});

connection.connect();

connection.query('SELECT 1+1 AS solution', function(err, rows, fields) {
  if (err) throw err;

  var connection2 = mysql.createConnection({
    host     : 'localhost',
    user     : argv['user'],
    password : argv['password']
  });

  connection2.connect();
  connection2.query('SELECT user(),current_user() AS solution', function(err, rows, fields) {
    if (err) throw err;

    console.log('The solution is: ', rows[0].solution, '(inside nested connection)');
  });

  connection2.end();
});

connection.end();
