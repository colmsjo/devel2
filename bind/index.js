f = function() {
  this.a = 'a';
  this.b = 1;
};

f.prototype.get = function() {
  return {a: this.a, b: this.b}
};

var o = new f();

console.log('call(null):', o.get.call(null));
console.log('call(this):', o.get.call(this));
console.log('call(o):', o.get.call(o));

console.log('bind(null):', o.get.bind(null)() );
console.log('bind(this):', o.get.bind(this)() );
console.log('bind(o):', o.get.bind(o)() );

console.log('bind(null).call(null):', o.get.bind(null).call(null) );
console.log('bind(null).call(o):', o.get.bind(null).call(o) );
console.log('bind(o).call(null):', o.get.bind(o).call(null) );
console.log('bind(o).call(this):', o.get.bind(o).call(this) );

