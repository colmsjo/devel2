Install the tools globally:

		npm install -g cordova
		npm install -g ios-sim
		npm install -g ios-deploy


Create and build hello:

	cordova create hello com.gizur.hello "HelloWorld"
	cd hello
	cordova platform add ios
	cordova prepare


Run the app in the emulator:

		cordova emulate ios
		tail platforms/ios/cordova/console.log

Start Safari and select `Developer->iOS simulator->HelloWorld->index.html`


Run the app in a device:

		cordova run ios --device


Links:

https://developer.apple.com/library/prerelease/ios/documentation/IDEs/Conceptual/AppDistributionGuide/LaunchingYourApponDevices/LaunchingYourApponDevices.html
