var http = require('http');
var url = require('url');
var util= require('util');

// empty constructor
var mws = function() {
  this.mw_ = [];
}

//
// Public functions
// ----------------

mws.prototype.use = function(route, handler) {
  var self = this;

  if(typeof route !== 'string') {
    handler = route;
    route = '/';
  }

  // remove trailing slash if there is one
  if(route.length > 1 && route[route.length-1] === '/') {
    route = route.substring(0, route.length-1);
  }

  self.mw_.push({route: route, handler: handler});
}

mws.prototype.httpFunc = function(req, res) {
  var self = this;

  var next = function() {

    if (self.index_ >= self.mw_.length) {
      return;
    }

    var mw = self.mw_[self.index_++];

    // check that the route matches
    if (self.pathname_.substr(0, mw.route.length).toLowerCase() !==
        mw.route.toLowerCase()) {

      next();
      return;
    }
    // make sure path ends with / or . if it is longer than the route
    var c = self.pathname_[mw.route.length];

    if (c !== undefined && c !== '/' && c !== '.' && mw.route !== '/') {
      next();
      return;
    }

    mw.handler(self.req_, self.res_, next);

  }

  self.pathname_ = url.parse(req.url, false, false).pathname;
  self.req_ = req;
  self.res_ = res;

  // make sure there is a slash in the begininng
  if (self.pathname_[0] !== '/') {
    self.pathname_ = '/' + self.pathname_;
  }

  self.index_ = 0;
  next();
};

mws.prototype.start = function() {
  var self = this;

  var server = http.createServer(self.httpFunc.bind(self));
  server.listen(3000);

  console.log('listening on port 3000');
}

module.exports = mws;
