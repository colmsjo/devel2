var util = require('util');
var middleware = require('./index.js');

var mws = new middleware();

console.log('--- nothing should be printed two lines below');
mws.httpFunc({url: 'http://example.com:9000/first/second?query=query#hash'},{});

mws.use('/first', function(req, res, next) {
  console.log('Matched /first - got request: ', req.url);
  next();
});

mws.use(function(req, res, next) {
  console.log('Matched / - got request: ', req.url);
  next();
});

mws.use(function(req, res, next) {
  console.log('Closing response stream');
  if(res.end) {
    res.end();
  }
  next();
});

console.log('--- there should be two lines below');
mws.httpFunc({url: 'http://example.com:9000/first/second?query=query#hash'},{});

console.log('--- there should be one line below');
mws.httpFunc({url: 'http://example.com:9000/fi/second?query=query#hash'},{});

console.log('--- there should be one line below');
mws.httpFunc({url: 'http://example.com:9000/firsttt/second?query=query#hash'},{});

console.log('---');

console.log('--- Run some test doing curl http://localhost:3000/...');
console.log('--- Then stop with ctrl-C');


mws.start();
