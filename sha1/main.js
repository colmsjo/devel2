jsSHA = require("jssha");


var getUserId = function(email) {
  var shaObj = new jsSHA(email, "TEXT");
  var hash = shaObj.getHash("SHA-1", "HEX");
  var hash1 = hash.substring(0,12);
  return  hash1.slice(0,4) + '-' + hash1.slice(4,8) + '-' + hash1.slice(8,12);

}

console.log(getUserId('jonas@gizur.com'));
console.log(getUserId('jonass@gizur.com'));
console.log(getUserId('test@gizur.com'));
