var xhr = function (url, method, data, cb) {
  if (!cb) {
    throw 'ERROR: Mandatory callback param not provided in H.xhr';
    return;
  }

  var messageToLog = function (evt) {
    var msg = 'Event occured: ' + JSON.stringify(evt) + '. ';
    msg += 'Current status: ' + oReq.status + '. ';
    msg += 'Current responseText: ' + oReq.responseText + '. ';
    msg += 'getAllResponseHeaders: ' + oReq.getAllResponseHeaders() + '. ';
    return msg;
  };

  var oReq = new XMLHttpRequest();
  var updateProgress, transferComplete, transferFailed, transferCanceled;
  updateProgress = transferCanceled = transferFailed =
    function (evt) {
      console.log('transferCanceled/transferFailed' + messageToLog(evt), 'ERROR');
    };

  transferComplete = function (evt) {
    if (oReq.status !== 200) {
      console.log('transferComplete' + messageToLog(evt), 'ERROR',
        'STATUS_' + oReq.status);
      cb({
        status: oReq.status,
        data: oReq.responseText
      });
      return;
    }

    cb({
      status: oReq.status,
      data: oReq.responseText
    });
  };

  //oReq.addEventListener("progress", updateProgress, false);
  oReq.addEventListener("load", transferComplete, false);
  oReq.addEventListener("error", transferFailed, false);
  oReq.addEventListener("abort", transferCanceled, false);

  try {
    oReq.open(method, url, true);

    if (method === 'GET') {
      console.log('doing GET');
      oReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    }

    if (method === 'POST') {
      console.log('doing POST');
      oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    if (data !== null) {
      oReq.send(data);
    } else {
      oReq.send();
    }
  } catch (e) {
    console.log('Error in server request: ' + e, 'ERROR', oReq.status);
  }
};

// curl -v http://31.192.225.22/ryoulive-api-test/public/v1/users/1?email=test@example.com\&password=test
// curl -v http://31.192.225.22/ryoulive-api-test/public/v1/users/1?email=\&password=
var login = function () {
  var email = document.getElementById('email').value;
  var password = document.getElementById('password').value;

  var params = 'email=' + email + '&password=' + password;
  var url = 'http://31.192.225.22/ryoulive-api-test/public/v1/users/1?' + params;

  console.log('login: ' + email + ', password: ' + password);
  console.log('url: ' + url);

  xhr(url, 'GET', null, function (res) {
    if (res.status != 200) {
      var data = JSON.parse(res.data);
      document.getElementById("loginMessage").innerHTML = "Can't login: " + data.detail;
      return;
    }
    console.log('Successful login');
    showContinue();
  });
}

// curl -v -X POST --data "email=test@example.com&password=test" http://31.192.225.22/ryoulive-api-test/public/v1/users
var register = function () {
  console.log('register');

  var email = document.getElementById('regEmail').value;
  var password = document.getElementById('regPassword').value;
  var repeatPassword = document.getElementById('regRepeatPassword').value;

  if (password !== repeatPassword) {
    document.getElementById("regMessage").innerHTML = "Passwords don't match";
    return;
  }

  var data = 'email=' + email + '&password=' + password;
  var url = 'http://31.192.225.22/ryoulive-api-test/public/v1/users';

  console.log('data: '+ data);

  xhr(url, 'POST', data, function (res) {
    if (res.status !== 200) {
      // TODO: show error message
      console.log("Can't register");
      var data = JSON.parse(res.data)
      document.getElementById("regMessage").innerHTML = "Can't register: " + data.detail;
      return;
    }
    console.log('Successful registration');
    window.localStorage.setItem('email', email);
    window.localStorage.setItem('password', password);
    showContinue();
  });
}

var showLogin = function () {
  document.getElementById("registerScreen").style.visibility = "hidden";
  document.getElementById("loginScreen").style.visibility = "visible";
  document.getElementById("continueScreen").style.visibility = "hidden";

  var email = window.localStorage.getItem('email');
  var password = window.localStorage.getItem('password');

  if (email !== null && password !== null) {
    document.getElementById("email").value = email;
    document.getElementById("password").value = password;
  }

}

var showContinue = function () {
  document.getElementById("registerScreen").style.visibility = "hidden";
  document.getElementById("loginScreen").style.visibility = "hidden";
  document.getElementById("continueScreen").style.visibility = "visible";

  var email = window.localStorage.getItem('email');
  document.getElementById("email2").value = email;
}

var showRegister = function () {
  document.getElementById("registerScreen").style.visibility = "visible";
  document.getElementById("loginScreen").style.visibility = "hidden";
  document.getElementById("continueScreen").style.visibility = "hidden";

  document.getElementById("regMessage").innerHTML = "";
}

var init = function () {
  document.getElementById("registerScreen").style.visibility = "hidden";
  document.getElementById("loginScreen").style.visibility = "hidden";

  console.log('init');

  document.getElementById('login').addEventListener('click', login);
  document.getElementById('showRegister').addEventListener('click', showRegister);
  document.getElementById('showRegister2').addEventListener('click', showRegister);
  document.getElementById('cancel').addEventListener('click', showLogin);
  document.getElementById('register').addEventListener('click', register);
  document.getElementById('changeAccount').addEventListener('click', showLogin);

  ['email', 'password', 'regEmail', 'regEmail', 'regPassword',
    'regRepeatPassword'
  ].forEach(function (id) {
    document.getElementById(id).addEventListener('focus', function (el) {
      el.target.value = '';
    });
  });

  showLogin();
}

var app = {
  // Application Constructor
  initialize: function () {
    this.bindEvents();
    //init();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function () {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function () {
    app.receivedEvent('deviceready');
    document.getElementById("deviceready").style.visibility = "hidden";
    showLogin();
  },
  // Update DOM on a Received Event
  receivedEvent: function (id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector('.listening');
    var receivedElement = parentElement.querySelector('.received');

    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');

    console.log('Received Event: ' + id);
  }
};

app.initialize();
document.addEventListener('DOMContentLoaded', init);
