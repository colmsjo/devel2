var vm = require('vm');
var util = require('util');

var sandbox = { globalVar: 1, require:require, console:console, setTimeout:setTimeout };
var context = vm.createContext(sandbox);

var script = new vm.Script('setTimeout(function() {console.log(typeof require);}, 1000)', { filename: 'myfile.vm' });
script.runInContext(context);

console.log(util.inspect(context));
