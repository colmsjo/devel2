var util = require('util');
var vm = require('vm');

var sandbox = { globalVar: 1 };
var context = vm.createContext(sandbox);

for (var i = 0; i < 10; ++i) {
    vm.runInContext('globalVar *= 2; foo = "bar"', context);
}

console.log(util.inspect(context));
