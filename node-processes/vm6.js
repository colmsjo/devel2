var vm = require('vm');
var util = require('util');

var sandbox = { globalVar: 1, require:require, console:console };
var context = vm.createContext(sandbox);

var script = new vm.Script('var u=require("util"); u.log(typeof require)', { filename: 'myfile.vm' });
script.runInContext(context);

console.log(util.inspect(context));
