var util = require('util');
var vm = require('vm');

var myContext = {
}
myContext.doLog = function(text) {
	util.log(text);
}

vm.runInNewContext('doLog("Hello World");', myContext);
