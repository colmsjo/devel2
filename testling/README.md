Example use of testling.com


  npm install tape
  npm install browserify
  ./node_modules/.bin/browserify test/*.js > test_bundle.js
  npm install testling
  ./node_modules/.bin/browserify test/testlingExample.js | ./node_modules/.bin/testling


Setup hook:

 * https://github.com/colmsjo/devel2/settings/hooks


Badge:

  * https://ci.testling.com/colmsjo/devel2.png
