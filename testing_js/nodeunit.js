exports['test.mysql'] = {

  'testing setTimeout': function(test) {
    test.expect(2);

    setTimeout(function() {
      test.ok(true, 'setTimeout executed...');
      test.done();
    }.bind(this), 1000);

  }

};
