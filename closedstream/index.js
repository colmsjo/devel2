var assert = require('assert');
var http = require('http');
var util = require('util');
var deepcopy = require('deepcopy');



var server = http.createServer(function(req, res) {

  resCopy = deepcopy(res);

  console.log('--- processing %s', req.url);
  console.log(util.inspect(res, {
          showHidden: true,
          depth: null,
          colors: true
        }));

  res.write('Nothing to see here!!');
  res.end();

  console.log('--- response ended');
  console.log(util.inspect(res, {
          showHidden: true,
          depth: null,
          colors: true
        }));


  try {
    assert.deepEqual(res, resCopy, 'difference found');
  } catch(e) {
    console.log(e);
  }

});

server.listen(3000);
console.log('Run some tests with curl http://127.0.0.1:3000');
