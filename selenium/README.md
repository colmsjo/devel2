
Firefox example:

1. `npm install -g mocha`
1. `npm install selenium-webdriver`
1. Start a web server: `../static-web-server.js`
1. `mocha test3.js`


Multiple browsers:

1. `SELENIUM_BROWSER=phantomjs mocha test4.js`
1. SELENIUM_BROWSER=chrome mocha test4.js
 * chromedriver needs to be installed, see Error message if this isn't the case

 1. Download selenium-server-standalone from http://selenium-release.storage.googleapis.com/index.html?path=2.46/
 1. extract the jar with `jar -xf ...`
1. Install the safari extension that is located in org.openqa.safari by dragging
it to the extensions tab in the preferences dialog
