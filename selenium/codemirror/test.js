// npm install selenium-webdriver codemirror
// mocha test.js

var By = require('selenium-webdriver').By,
  until = require('selenium-webdriver').until,
  firefox = require('selenium-webdriver/firefox'),
  test = require('selenium-webdriver/testing'),
  webdriver = require('selenium-webdriver'),
  phantomjs = require('selenium-webdriver/chrome'),
  chrome = require('selenium-webdriver/chrome');

var assert = require('assert');

var text = 'webdriver';

test.describe('Test my own app', function() {
  var driver;

  this.timeout(10000);

  test.before(function() {

    var profile = new firefox.Profile().setNativeEventsEnabled(true);
    var options = new firefox.Options().setProfile(profile);

//    driver = new firefox.Driver();

    driver = new webdriver.Builder()
      .forBrowser('firefox')
      .setChromeOptions(  )
      .setFirefoxOptions( options )
      .build();

  });

  test.after(function() {
    driver.quit();
  });

  test.it('should just type something the input field', function(done) {
    driver.get('http://127.0.0.1:8125/ide.html');

      new webdriver.ActionSequence(driver).
          click(driver.findElement(By.id('clear'))).
          sendKeys(webdriver.Key.TAB).
          sendKeys(text).
          perform();

    driver.executeScript('return window.editor.getValue()')
    .then(function(res) {
      assert.equal(res, text, 'editor.getValue should equal the input text');
      done()
    });

  });
});
