var editor;

var init = function() {
  console.log('ide:init');

  var cm = document.getElementById('cm');

  editor = editor || new CodeMirror(cm, {
    mode: "text/html",
    styleActiveLine: true,
    lineNumbers: true,
    lineWrapping: true
  });

  window.editor = editor;

};

var clear = function() {
  console.log('ide:clear');
  editor.setValue('');
};

document.addEventListener('DOMContentLoaded', init);
document.getElementById('clear').addEventListener('click', clear);
