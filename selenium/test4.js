var By = require('selenium-webdriver').By,
  until = require('selenium-webdriver').until,
  firefox = require('selenium-webdriver/firefox'),
  test = require('selenium-webdriver/testing');


var webdriver = require('selenium-webdriver'),
  phantomjs = require('selenium-webdriver/chrome'),
  chrome = require('selenium-webdriver/chrome'),
  firefox = require('selenium-webdriver/firefox');



test.describe('Test my own app', function() {
  var driver;

  this.timeout(10000);

  test.before(function() {
//    driver = new firefox.Driver();
    driver = new webdriver.Builder()
    .forBrowser('firefox')
    .setChromeOptions(/* ... */)
    .setFirefoxOptions(/* ... */)
    .build();

  });

  test.after(function() {
    driver.quit();
  });

  test.it('should just type something the input field', function(done) {
    driver.get('http://127.0.0.1:8125/');
    driver.findElement(By.id('in')).sendKeys('webdriver');

    driver.findElement(By.id('out')).getAttribute("value")
      .then(function(res) {
        console.log('out: ' + res);

        // Wait to make it possible to see the browser
        setTimeout(done, 5000);
      });

    //driver.wait(until.titleIs('webdriver - Google Search'), 1000);
  });
});
