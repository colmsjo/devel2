//
// example1.js
//
// Start process: `npm install dtrace-provider; node example1.js`
//
// See the `dtrace` output:
// `sudo dtrace -Z -n 'nodeapp*:::probe1{ trace(arg0); trace(arg1) }' -n 'nodeapp*:::probe2{ trace(copyinstr(arg0));  }'`
//
//
//
//

var d = require('dtrace-provider');

var dtp = d.createDTraceProvider("nodeapp");
var p1 = dtp.addProbe("probe1", "int", "int");
var p2 = dtp.addProbe("probe2", "char *");
dtp.enable();


// Fire the probes every 3 seconds
setInterval(function() {

  console.log('Firing dtrace probe1 and probe2');

  dtp.fire("probe1", function(p) {
    return [1, 2];
  });
  dtp.fire("probe2", function(p) {
    return ["hello, dtrace via provider", "foo"];
  });

}, 3000);
