//
// rest1.js
//
// start server: npm install restify; node rest1.js
// perform http request: curl -s http://0.0.0.0:8080/hello/dtrace
//
// See the `dtrace` output:
// `sudo dtrace    -n 'node*:::http-server-request{ printf("%s: %s of %s\n", zonename, args[0]->method, args[0]->url) }'`
// `sudo dtrace    -n 'node*:::http-server-request{ printf("%s: %s of %s\n", zonename, trace(args[0])->method, trace(args[0])->url) }'`
// `sudo dtrace -Z -n 'node*:::http-client-request{ trace(arg0); trace(arg1) }' `


var server = require('restify').createServer({
  name: 'foo'
});

server.use(function slowDown(req, res, next) {
  setTimeout(function () {
    return next();
  }, 513);
});

server.get('/hello/:name', function echo(req, res, next) {
  res.send('hello ' + req.params.name);
  return next();
});

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});
