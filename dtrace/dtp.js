// npm install dtrace-provider
// node node.js
// sudo dtrace -Z -n 'nodeapp*:::probe{ trace(copyinstr(arg0)); }'

var d = require('dtrace-provider');


var dtp = d.createDTraceProvider("nodeapp");

// Can use JSON probe but it does not seam to work in OSX: var p1 = dtp.addProbe("probe", "json");
var p1 = dtp.addProbe("probe", "char *");

dtp.enable();


// Fire probe every 3 seconds
setInterval(function() {

  console.log("Fire probe!")

// This does not work on OSX with node v0.10.33
//  dtp.fire("probe1", function(p) { return { "foo": "bar" }; });

  // This does work
  dtp.fire("probe", function(p) {
    return [JSON.stringify({ foo: 'bar' }), "foo"];
  });

}, 3000);
