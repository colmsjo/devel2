/*
 * counter.d
 *
 * Count off and report the number of seconds elapsed
 *
 * dtrace -s counter.d
 *
 */

dtrace:::BEGIN
{
        i = 0;
}

/* The profile provider makes it possible to create new 5
 *
 * Some options available:
 *
 * profile:::tick-100msec
 * profile:::tick-1sec
 *
 */

profile:::tick-100msec
{
        i = i + 1;
        trace(i);
}

dtrace:::END
{
        trace(i);
}
