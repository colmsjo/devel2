using dtrace in NodeJS
======================

`dtrace` is a very powerfull tool that is useful when working with node. It is
a general tracing tool developed by Sun Microsystems that gradually gained support
in several OS:es and programming languages.

dtrace provider for node: `npm install dtrace-provider`. See more details at
[node-dtrace-provider](https://github.com/chrisa/node-dtrace-provider) which is
based on [libusdt](https://github.com/chrisa/libusdt).


Howto check for probes
----------------------

`dtrace` probes are only possible to see when the process of interest is running.

 * start a process, for instance: `node rest1.js`
 * list all probes in the node dtrace module:  `sudo dtrace -l -m node`
 * You'll see that the dtrace module is named like this node[PID]

MySQL also provides a large number of `dtrace` probes: `sudo dtrace -l -m mysqld`.
There is are reference [here](http://dev.mysql.com/doc/refman/5.5/en/dba-dtrace-mysqld-ref.html)


Introduction to dtrace
----------------------

There are probes in the kernel that can be used for tracing and debugging.
You can also add your own probes if there is `dtrace` provider for your language.

You can run `sudo dtrace -l` if `dtrace` is supported for your OS (Solaris, OSX, linux,
FreeBSD, NetBSD) and you'll get a (long) list of available probes. `dtrace` is
built around a the scripting language `D` which is a subset if `C`.

Some introductions for `dtrace` are available here:

 * [dtrace basics](https://docs.oracle.com/cd/E18752_01/html/819-5488/gbxwv.html)
 * [dtrace introduction](https://wikis.oracle.com/display/DTrace/Introduction)

 * [Beauchamp-Weston](https://www.blackhat.com/presentations/bh-europe-08/Beauchamp-Weston/Presentation/bh-eu-08-beauchamp-weston.pdf)

 * [NodeJS and dtrace](http://dtrace.org/blogs/bmc/2010/08/30/dtrace-node-js-and-the-robinson-projection/)

 * [Memory leaks](http://dtrace.org/blogs/bmc/2012/05/05/debugging-node-js-memory-leaks/)

 * [Stack trace](http://dtrace.org/blogs/dap/2013/11/20/understanding-dtrace-ustack-helpers/)

 * [dtrace internals for node](http://cdn.oreillystatic.com/en/assets/1/event/60/Instrumenting%20the%20real-time%20web_%20Node_js,%20DTrace%20and%20the%20Robinson%20Projection%20Presentation.pdf)


`dtrace` scripts that are shipped with OSX can be listed like this: `man -k dtrace`
