//
// Test to see if `!arg` works in the same way as `typeof arg === undefined`
//

var f = function(a) {
  if(!a) console.log('no argument given');
  if(typeof a === 'undefined') console.log('no argument given');
  if(a) console.log('argument is given');
};

var g = function(o) {
  if(!o || !o.a) console.log('no argument given');
  if(typeof o === 'undefined' || typeof o.a === 'undefined') console.log('no argument given');
};

console.log('should see one line below:');
f('one');
console.log('should see two lines below:');
f();

console.log('should see four lines below:');
g({a: 'one'});
g({});
g();
