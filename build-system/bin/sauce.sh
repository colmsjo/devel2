#!/bin/bash
source ../setenv

# Run sample test
#curl -X POST https://saucelabs.com/rest/v1/browser_tests2/js-tests \
#-u $SAUCE_USERNAME:$SAUCE_ACCESS_KEY \
#-d platforms='[["Windows 8", "internet explorer", "10"], ["OS X 10.8", "safari", "6"]]' \
#-d url="https://saucelabs.com/test_helpers/front_tests/index.html" \
#-d framework=jasmine -d name=sauce-sample-test

TEST_URL=https://raw.githubusercontent.com/colmsjo/devel2/master/gulp/test/test.html

curl -X POST https://saucelabs.com/rest/v1/browser_tests2/js-tests \
-u $SAUCE_USERNAME:$SAUCE_ACCESS_KEY \
-d platforms='[["Windows 8", "internet explorer", "10"], ["OS X 10.8", "safari", "6"]]' \
-d url="$TEST_URL" \
-d framework=jasmine -d name=mocha
