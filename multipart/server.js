var http = require('http');
var util = require('util');

http.createServer(function(req, res) {


  req.on('data', function(chunk) {
    console.log('\nreceived chunk:\n', chunk.toString());
  });

  console.log('headers:\n', util.inspect(req.headers, { showHidden: true, depth: null, colors: true }));
  res.write('Hello');
  res.end();

}).listen(3000);
