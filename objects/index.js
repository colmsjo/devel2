var util = require('util');

var merge = function(src, dest) {

  for (var key in src) {

    // merge nested objects
    if (typeof src[key] === 'object') {
      if(!dest[key]) dest[key] = {};
      merge(src[key], dest[key]);
    }

    // add properties that don't exist
    if (!dest.hasOwnProperty(key)) {
      console.log('Adding property: ', key);
      dest[key] = src[key];
    }
  }
}

var o1 = {
  a: 'a'
};

var o2 = {
  b: 'b'
};

merge(o1,o2);
console.log(util.inspect(o2));

o1['o'] = {one: 1};
merge(o1,o2);
console.log(util.inspect(o2));

var o3 = {a: '2'};
console.log(util.inspect( o3.concat(o2) ));

