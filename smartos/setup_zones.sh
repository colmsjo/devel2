#!/bin/bash
 
echo "-- Importing base SmartOS image..."
imgadm import 62f148f8-6e84-11e4-82c5-efca60348b9f
 
echo "-- Create zones..."
cd /opt
 
cat > firewall.json <<EOL
{
  "alias": "gateway",
  "hostname": "gateway",
  "brand": "joyent",
  "quota": 10,
  "max_physical_memory": 1024,
  "dataset_uuid": "62f148f8-6e84-11e4-82c5-efca60348b9f",
  "default_gateway": "192.168.1.1",
  "resolvers": [
    "8.8.8.8",
    "8.8.4.4"
  ],
  "nics": [
    {
      "nic_tag": "admin",
      "ip": "192.168.1.20",
      "netmask": "255.255.255.0",
      "allow_ip_spoofing": "1",
      "gateway": "192.168.1.1",
      "primary": "1"
    },
    {
      "interface": "net1",
      "mac": "92:f6:41:4d:72:44",
      "nic_tag": "stub0",
      "gateway": "10.0.0.1",
      "ip": "10.0.0.1",
      "netmask": "255.255.255.0",
      "allow_ip_spoofing": "1"
    }
  ]
}
EOL
 
cat > client.json <<EOL
{
  "alias": "scottenfinn",
  "hostname": "scottenfinn",
  "brand": "joyent",
  "quota": 10,
  "max_physical_memory": 1024,
  "dataset_uuid": "62f148f8-6e84-11e4-82c5-efca60348b9f",
  "default_gateway": "10.0.0.1",
  "resolvers": [
    "8.8.8.8",
    "8.8.4.4"
  ],
  "nics": [
    {
      "nic_tag": "stub0",
      "ip": "10.0.0.2",
      "netmask": "255.255.255.0",
      "allow_ip_spoofing": "1",
      "gateway": "10.0.0.1"
    }
  ]
}
EOL
 
nictagadm add -l stub0
vmadm create -f firewall.json &
vmadm create -f client.json
 
echo "-- Setup boot scripts"
 
# Setup a script that is executed at boot (the OS is read-only to configurations are wiped out at boot)
mkdir -p /opt/custom/smf/
 
cat > /opt/custom/smf/systemprep.xml<<EOL
<?xml version='1.0'?>
<!DOCTYPE service_bundle SYSTEM '/usr/share/lib/xml/dtd/service_bundle.dtd.1'>
<service_bundle type='manifest' name='export'>
  <service name='smartos/setup' type='service' version='0'>
    <create_default_instance enabled='true'/>
    <single_instance/>
    <dependency name='net-physical' grouping='require_all' restart_on='none' type='service'>
      <service_fmri value='svc:/network/physical'/>
    </dependency>
    <dependency name='filesystem' grouping='require_all' restart_on='none' type='service'>
      <service_fmri value='svc:/system/filesystem/local'/>
    </dependency>
    <exec_method name='start' type='method' exec='/opt/custom/share/svc/smartos_setup.sh %m' timeout_seconds='0'/>
    <exec_method name='stop' type='method' exec='/opt/custom/share/svc/smartos_setup.sh %m' timeout_seconds='60'/>
    <property_group name='startd' type='framework'>
      <propval name='duration' type='astring' value='transient'/>
    </property_group>
    <stability value='Unstable'/>
    <template>
      <common_name>
        <loctext xml:lang='C'>SmartOS Ad Hoc Setup Script</loctext>
      </common_name>
    </template>
  </service>
</service_bundle>
EOL
 
mkdir -p /opt/custom/share/svc
cat > /opt/custom/share/svc/smartos_setup.sh<<EOL
#!/bin/bash
# Simple Ad Hoc SmartOS Setup Service
 
set -o xtrace
 
. /lib/svc/share/smf_include.sh
 
cd /
PATH=/usr/sbin:/usr/bin:/opt/custom/bin:/opt/custom/sbin; export PATH
 
case "$1" in
'start')
    #### Insert code to execute on startup here.
 
#    hostname "timboudreau.com" && hostname > /etc/nodename
    echo "export PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '" >> /root/.bashrc
    echo "export TERM=vt100" >> /root/.bashrc 
    echo "export PATH=$PATH:/opt/bin"
    echo "alias ls='ls --color=auto'" >> /root/.bashrc
    echo 'alias vl="vmadm list"' >> /root/.bashrc
#    cp opt/custom/share/ipnat.conf /etc/ipf/
    routeadm -u -e ipv4-forwarding
#    svcadm restart ipfilter
    ln -s /usr/share/vim/vim73/vimrc_example.vim /root/.vimrc
    ;;
 
'stop')
    ### Insert code to execute on shutdown here.
    ;;
 
*)
    echo "Usage: $0 { start | stop }"
    exit $SMF_EXIT_ERR_FATAL
    ;;
esac
exit $SMF_EXIT_OK
EOL
chmod ug+x /opt/custom/share/svc/smartos_setup.sh
 
echo "-- Reboot for the changes to take effect..."
