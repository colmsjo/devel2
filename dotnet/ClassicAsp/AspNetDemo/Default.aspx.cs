﻿using System;
using System.Web;
using System.Web.UI;

namespace AspNetDemo
{
	
	public partial class Default : System.Web.UI.Page
	{
		public void button1Clicked (object sender, EventArgs args)
		{
			button1.Text = "You clicked me";
		}

		public void clickMeButton_Click (object sender, EventArgs e)
		{
			object val = ViewState["ButtonClickCount"];
			int i = (val == null)? 1 : (int)val + 1;
			outputlabel.Text = string.Format ("You clicked me {0} {1}", i, i==1?"time":"times");
			ViewState["ButtonClickCount"] = i;
		}
	}
}

