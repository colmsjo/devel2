I'm using Mono for .NET development.

Console app:

1. Install [Mono project](http://www.mono-project.com)
2. Compile hello.cs: `mcs hello.cs`
3. Run hello.exe: `mono hello.exe`

ASP.NET app:

1. Start the web server: `xsp4 --port 9000`



