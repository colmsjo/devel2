1. Build the server: `xbuild` (make sure that dependencies are installed)
2. Start the server: `mono ...` 
3. Call API: `curl http://localhost:9000/api/hello`
