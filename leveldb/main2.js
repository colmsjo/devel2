//
// sudo node -e "require('./main2.js').start();"
//
//

//
// npm install levelup
// npm install leveldown
// npm install async
//
// NOTE: npm install level seams broken. It is using a older version of leveldb than leveldown
//



(function(self_, undefined) {

  var mos = self_.mysqlodata || {};
  var u = require('underscore');
  var async = require('async');

  // change to false to stop this logging
  var debug = true,
    info = true,
    noLogging = false;

  var defaultPort = 80;

  //
  // Helpers
  // ===================

  var log = {

    debug: function(text) {
      if (debug && !noLogging) console.log('DEBUG: ' + text);
    },

    info: function(text) {
      if (info && !noLogging) console.log('INFO: ' + text);
    },

    log: function(text) {
      if (!noLogging) console.log(text);
    }
  };

  // a - the number to convert
  // b - number of resulting characters
  var pad = function(a, b) {
    return (1e15 + a + "").slice(-b);
  };

  //
  // Base class for Adapters
  // =======================
  //

  var BackendAdapter = function() {
  };

  BackendAdapter.prototype.on_data = function(chunk, request, response) {
    throw new Error('BackendAdapter is abstract. on_data() needs to be implemented');
  };

  BackendAdapter.prototype.on_end = function(request, response) {
    throw new Error('BackendAdapter is abstract. on_end() needs to be implemented');
  };


  //
  // Leveldb
  // =======
  //
  // Store data/blobs in chunks in the database. Keys have the following form: key~rev#~chunk#
  // rev# and chunk# are 9 digits key~000000001~000000001
  //

  var LevelDB = function() {

    var self = this;

    //
    // Privileged properties
    // ---------------------

    self._data        = '';
    self._postCounter = 0;
    self._getCounter  = 0;
    self._rev         = -1;
    self._req         = null;
    self._res         = null;
    self._lastChunk   = -1;


    // These are initialized in the init function
    self.levelup      = null;
    self.leveldb      = null;


    //
    // Helpers
    // -------

    // maximum 999.999.999 revisions and 999.999.999 chunks
    self._readKeys = function(keyPrefix, cb) {

      var _keyStream = self.leveldb.createReadStream({
        start: keyPrefix + '~000000000',
        end: keyPrefix + '~999999999',
        limit: 999999999,
        reverse: false,
        keys: true,
        values: false
      });

      var _keys = [];

      _keyStream.on('data', function(data) {
        _keys.push(data);
      });

      _keyStream.on('error', function(err) {
        log.log('Error reading leveldb stream: ' + err);
      });

      _keyStream.on('close', function() {
        cb(_keys);
      });
    };

    // maximum 999.999.999 revisions and 999.999.999 chunks
    self._readValue = function(keyPrefix, revision, cbData, cbEnd) {

      var _revision = pad(revision, 9);

      var _keyStream = self.leveldb.createReadStream({
        start: keyPrefix + '~' + _revision + '~000000000',
        end: keyPrefix + '~' + _revision + '~999999999',
        limit: 999999999,
        reverse: false,
        keys: false,
        values: true
      });

      _keyStream.on('data', function(data) {
        cbData(data);
      });

      _keyStream.on('error', function(err) {
        log.log('Error reading leveldb stream: ' + err);
      });

      _keyStream.on('close', function() {
        cbEnd();
      });
    };

    // Get the last revison of a key that is stored in the database
    self._getCurrentRev = function(keyPrefix, cb) {

      // revision is already fetched if rev > -1
      if (self._rev > -1) {
        log.debug('LevelDB.getCurrentRev: RETURNING, keyPrefix=' + keyPrefix +
          ', rev= ' + self._rev);

        if (cb !== null)  cb(null, '_getCurrentRev');
        return;
      }

      self._readKeys(keyPrefix, function(keys) {

        if (keys.length > 0) {
          var _revs = u.map(
            keys,
            function(k) {
              return k.slice(keyPrefix.length + 1, keyPrefix.length + 1 + 9);
            }
          );

          self._rev = parseInt(u.max(_revs, function(r) { return parseInt(r); }));

          log.debug('LevelDB.getCurrentRev: keyPrefix=' + keyPrefix + ', rev= ' +
            self._rev);

        }

        // end of async series
        if (cb !== null)  cb(null, '_getCurrentRev');

      });
    };

    self._formatKey = function(k, revNum, chunkNum) {
      return k + '~' + pad(revNum, 9) + '~' + pad(chunkNum, 9);
    };


    //
    // HTTP Server helpers
    // ====================

    // Respond with 406 and end the connection
    self._writeError = function(response, err) {
      // Should return 406 when failing
      // http://www.odata.org/documentation/odata-version-2-0/operations/
      response.writeHead(406, {
        "Content-Type": "application/json"
      });
      response.write(err.toString() + '\n');
      response.end();

      log.log(err.toString());
    };

  };


  //
  // Public functions
  // ----------------

  // inherit BackendAdapter
  LevelDB.prototype = Object.create(BackendAdapter.prototype);

  LevelDB.prototype.init = function(request, response) {

    var self = this;

    // Open the LevelDB database
    self.levelup = require('level');
    self.leveldb = self.levelup('./mydb');

    // save for later use
    self._req        = request;
    self._res        = response;
    self._lastChunk  = -1;
    self._getCurrentRev(self._req.url, null);

    log.log('LevelDB.init: request=' +
      JSON.stringify(self._req.method) + " - " +
      JSON.stringify(self._req.url) + " - " +
      JSON.stringify(self._req.headers));

  };

  // Override on_data
  LevelDB.prototype.on_data = function(chunk) {

    var self = this;

    var _processReq = function(cb) {
      if (self._req.method == 'GET') {
        log.debug('LevelDB.on_data: url=' + self._req.url + ', rev= ' +
          self._rev + ', counter=' + self._getCounter);
      }

      // parse data and prepare insert for POST requests
      if (self._req.method == 'POST') {
        log.debug('LevelDB.on_data: url=' + self._req.url + ', rev= ' + self._rev +
          ', counter=' + self._postCounter + ', last chunk='+self._lastChunk);

        var _k = self._formatKey(self._req.url, self._rev, ++self._postCounter);

        self.leveldb.put(_k, chunk, function(err) {
          if (err) return log.log('LevelDB.onodata: error saving chunk!', err);
        });

        if(self._lastChunk === self._postCounter) {
          log.log('Finished saving: ' + self._req.url + ', new revision: ' +
                  self._rev + ', number of chunks: ' + self._postCounter);

        }

      }

      if (cb !== null) cb(null, 'processed data');

    };

    log.debug('LevelDB.on_data: request=' +
      JSON.stringify(self._req.method) + " - " +
      JSON.stringify(self._req.url) + " - " +
      JSON.stringify(self._req.headers));

      _processReq(null);


    // Fetch current revision first and then process the data
    /*async.series([

      // first get the current revision number
      function(cb) {
        self._getCurrentRev(self._req.url, cb);
      }.bind(self),

      // Then process the request
        _processReq.bind(self)
    ]);*/

  };

  // Override on_end
  LevelDB.prototype.on_end = function() {

    var self = this;

    try {

      // parse data and prepare insert for POST requests
      if (self._req.method == 'GET') {

        log.debug('LevelDB.on_end: in function for URL: ' + self._req.url);

        self._res.writeHead(200, {
          "Content-Type": "application/json"
        });

        self._readValue(self._req.url, self._rev,
          function(value) {
            self._getCounter++;
            self._res.write(value);
          },
          function() {
            log.log('Finished reading: ' + self._req.url + ', revision: ' +
            self._rev + ', number of chunks sent: ' + self._getCounter);
            self._res.end();
          });
      }

      // parse data and prepare insert for POST requests
      if (self._req.method == 'POST') {
        self._lastChunk = self._postCounter;

        log.debug('LevelDB.on_end: in function for URL: ' + self._req.url +
          ', number of chunks received: ' + self._lastChunk);

        self._res.writeHead(200, {
          "Content-Type": "application/json"
        });
        self._res.write('{status: ok}');
        self._res.end();

      }

    } catch (e) {
      writeError(self._res, e);
    }
  };


  //
  // Main
  // ====

  mos.start = function() {
    var http = require("http");

    var server = http.createServer(function(request, response) {

      var _buckets = new LevelDB();
      _buckets.init(request, response);

      // function(...) is needed to create a closure around _buckets
      request
        // read the data in the stream, if there is any
        .on('data', function(chunk) { _buckets.on_data(chunk); }.bind(_buckets))
        // request closed, process it
        .on('end', function() { _buckets.on_end(); }.bind(_buckets) );

    });

    server.listen(defaultPort);

    log.log("Server is listening on port " + defaultPort);

  };


  // Exports
  // =======

  module.exports = mos;

})(this);
