#!/bin/bash
# This script runs 10 parallel unit tests

# cleanup database
rm -rf ./mydb

# Start server
node -e "require('./main3.js').start();" &> ./runtests.log &
SERVER_PID=$!
echo "Wait for server to startup..."
sleep 3


i="0"
while [ $i -lt 10 ]
do
  ./node_modules/.bin/nodeunit unittest.js &
  i=$[$i+1]
done

# Wait a few seconds and then stop server
sleep 5
echo "Wait for tests to finish before stopping the server..."
kill $SERVER_PID
