http.on(end) seams to be emitted before http.on(data) has finished processing.
leveldb is async. Is saving to disk slower that the network? That would explain
why end is emitted before all data events are finished.

Need to call close in either last data event or in the end event!

Put all events on a queue and process. Wait until the end event is received
before processing starts.

The best solution is probably to pipe the http stream to leveldb.
By piping http into leveldb is separate keys for each chunk skipped.
Could just pipe http directly into leveldb.



httpInputStream.pipe( leveldb.WriteStream );


-------------

Working with Streams: https://github.com/substack/stream-handbook

Create writable stream:

  var Writable = require('stream').Writable;
  var ws = Writable();
  ws._write = function (chunk, enc, next) {
      console.dir(chunk);
      next();
  };


--------------

Create new class that inherits Writable:

var stream = require('stream');
var util = require('util');

function EchoStream () { // step 2
  stream.Writable.call(this);
};
util.inherits(EchoStream, stream.Writable); // step 1
EchoStream.prototype._write = function (chunk, encoding, done) { // step 3
  console.log(chunk.toString());
  done();
}

var myStream = new EchoStream(); // instanciate your brand new stream
process.stdin.pipe(myStream);

----------------

I like this way of inheriting better:

var Writable = require('stream').Writable;

var myWritableStream = function() {
  // inherit stream.Writeable
  this.prototype = Writable;

  // call stream.Writeable constructor
  this.Writable.call(this);

}
