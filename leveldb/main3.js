//
// main3.js
//
// npm install
// sudo node -e "require('./main3.js').start();"
//



(function(self, undefined) {

  var mos = self.mysqlodata || {};

  var u = require('underscore');
  var async = require('async');

  var h = require('./helpers.js');
  var level = require('./leveldb_streams.js');


  var defaultPort = 80;

  //
  // Main
  // ====

  mos.start = function() {
    var http = require("http");

    var server = http.createServer(function(request, response) {

      h.log.log("Processing request: " +
        JSON.stringify(request.method) + " - " +
        JSON.stringify(request.url) + " - " +
        JSON.stringify(request.headers));

      // handle request with leveldb
      var leveldb = new level.LevelDBHttpServer();
      leveldb.main(request, response);

    });

    server.listen(defaultPort);

    h.log.log("Server is listening on port " + defaultPort);

  };


  process.on('exit', function() {
    h.log.log('Closing leveldb and exiting process.');
    level.close();
  });

  process.on('SIGINT', function() {
    h.log.log('Got SIGINT signal.');
    process.exit();
  });


  // Exports
  // =======

  module.exports = mos;

})(this);
