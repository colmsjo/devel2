var webPage = require('webpage');
var page = webPage.create();

var url = 'http://127.0.0.1:8125/page.html';

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

var focus = function(elId) {
  page.evaluate(function(elId) {
    document.getElementById('user').focus();
  }, elId);
};

var click = function(elId) {
  page.evaluate(function(elId) {
    var simulateClick = function(elId) {
      var evt;
      var el = document.getElementById(elId);
      if (document.createEvent) {
        evt = document.createEvent("MouseEvents");
        evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      }
      (evt) ? el.dispatchEvent(evt) : (el.click && el.click());
    }

    simulateClick(elId);
  }, elId);
};

var type = function(str) {
  if (typeof str === 'undefined') throw 'argument is mandatory!';

  var toPhantomChar = function(c) {
    if (c === ' ') return page.event.key.Space;
    return page.event.key[c.toUpperCase()];
  }
  
  for(var i=0; i<str.length; i++) {
    var c = str[i];
    page.sendEvent('keypress', toPhantomChar(c), null, null, 0x02000000 | 0x08000000);
  }

}

var step1 = function() {
  console.log('simulate that form is entered with text and button clicked');

  page.evaluate(function() {
    document.getElementById('user').focus();
  });

  focus('user');
  type('hi there');
  click('btn')
};

page.open(url, function (status) {
  console.log('page load:' + status);

  var i=1;

  // wait fo the page to load
  setTimeout(step1.bind(this), (i++)*1000);

  setTimeout(function() {
    phantom.exit();

  }, (i++)*1000);
});
