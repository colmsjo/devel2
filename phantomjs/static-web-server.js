#!/usr/bin/env node

var http = require('http');
var fs = require('fs');
var path = require('path');

var argv = require('minimist')(process.argv.slice(2));

http.createServer(function(request, response) {

  var filePath = '.' + request.url;
  if (filePath == './')
    filePath = './index.html';

  console.log('serving: ' + filePath);
  //console.log('method: ' + JSON.stringify(request.method));
  //console.log('headers: ' + JSON.stringify(request.headers);

  var extname = path.extname(filePath);
  var contentType = 'text/html';
  switch (extname) {
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.json':
      contentType = 'application/json';
      break;
    case '.png':
      contentType = 'image/png';
      break;
    case '.jpg':
      contentType = 'image/jpg';
      break;
    case '.wav':
      contentType = 'audio/wav';
      break;
  }

  fs.readFile(filePath, function(error, content) {
    if (error) {
      if (error.code == 'ENOENT') {
        fs.readFile('./404.html', function(error, content) {
          response.writeHead(200, {
            'Content-Type': contentType
          });
          response.end(content, 'utf-8');
        });
      } else {
        response.writeHead(500);
        response.end('Sorry, check with the site admin for error: ' +
                     error.code + ' ..\n');
        response.end();
      }
    } else {
      response.writeHead(200, {
        'Content-Type': contentType,
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Access-Control-Allow-Credentials': 'true'
      });
      response.end(content, 'utf-8');
    }
  });

}).listen( (argv['port']) ? argv['port'] : 8125);
console.log('Server running at http://127.0.0.1:' +
            ((argv['port']) ? argv['port'] : 8125) + '/');
