var webPage = require('webpage');
var page = webPage.create();

var url = 'http://127.0.0.1:8125/iframe.html';

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

var focus = function(frameId, elId) {
  page.evaluate(function(frameId, elId) {
    var doc = document.getElementById(frameId).contentDocument;
    doc.getElementById(elId).focus();
  }, frameId, elId);
};

var click = function(frameId, elId) {
  page.evaluate(function(frameId, elId) {
    var simulateClick = function(frameId, elId) {
      var evt;
      var doc = document.getElementById(frameId).contentDocument;
      var el = doc.getElementById(elId);
      if (doc.createEvent) {
        evt = doc.createEvent("MouseEvents");
        evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      }
      (evt) ? el.dispatchEvent(evt) : (el.click && el.click());
    }

    simulateClick(frameId, elId);
  }, frameId, elId);
};

var type = function(str) {
  if (typeof str === 'undefined') throw 'argument is mandatory!';

  var toPhantomChar = function(c) {
    if (c === ' ') return page.event.key.Space;
    return page.event.key[c.toUpperCase()];
  }

  for(var i=0; i<str.length; i++) {
    var c = str[i];
    page.sendEvent('keypress', toPhantomChar(c), null, null, 0x02000000 | 0x08000000);
  }

}

var step1 = function() {
  console.log('simulate that form is entered with text and button clicked');

  focus('frm', 'user');
  type('hi there');
  click('frm', 'btn')
};

page.open(url, function (status) {
  console.log('page load:' + status);

  var i=1;

  // wait fo the page to load
  setTimeout(step1.bind(this), (i++)*1000);

  setTimeout(function() {
    phantom.exit();

  }, (i++)*1000);
});
