//
// Minimal test runner using observables that should work in any JavaScript
// environment. Works in NodeJs, PhantomJS and modern browsers.
//


//
// Observable
// ------------

var Observable = function(operation, arg) {
  this.operation_ = operation;
  if (operation === 'map') this.fn_ = arg;
  if (operation === 'merge') this.mergeWith_ = arg;
  this.observers_ = [];
};

Observable.prototype.register = function(observer) {
  this.observers_.push(observer);
};

Observable.prototype.notify = function(evt) {
  if (this.operation_ === 'map') {
    var tmpEvt = this.fn_(evt);
    evt = tmpEvt ? tmpEvt : evt;
  }

  this.observers_.forEach(function(o) {
    o.notify(evt);
  });
};

Observable.prototype.listen = function(element, event) {
  element.addEventListener(event, this.notify.bind(this));
};

Observable.fromEvent = function(element, event) {
  var observable = new Observable();
  observable.listen(element, event);
  return observable;
};

Observable.prototype.map = function(fn) {
  var observable2 = new Observable('map', fn);
  this.register(observable2);

  return observable2;
};

Observable.prototype.merge = function(observable2) {
  var observable3 = new Observable('merge', observable2);

  this.register(observable3);
  observable2.register(observable3);

  return observable3;
};

//
// Test runner
// ------------

// use setImmediate if it exists, otherwise process.nextTick,
// otherwise setTimeout(fn,0) or just call the function
var immediate = function(fn) {
  var run = (typeof setImmediate === 'function' && setImmediate) ||
    (typeof process === 'object' && process.nextTick) ||  
    (typeof setTimeout === 'function' && setTimeout) ||
    null;

  if (!run) return fn();
  if (run === setTimeout) {
    setTimeout(fn, 0);
    return;
  }
  return run(fn);
};

TestObservable = function() {
  Observable.call(this);
};

// inherit Observable
TestObservable.prototype = Object.create(Observable.prototype);

TestObservable.basicFormat = function(e) {
  if (e.stack) {
    return '-- Found error --\n' +
            'Stack trace:\n' + e.stack + '\n' +
            'Number if errors found: ' + e.numberOfErrors;
  } else return e;
};

TestObservable.test = function(fn) {
  observable = new TestObservable();

  immediate(function() {
    try {
      fn();
    } catch (e) {

      var msg = {
        error: e,
        numberOfErrors: ++TestObservable.numErrors_
      };

      if (e.lineNumber) msg['lineNumber'] = e.lineNumber;
      if (e.stack) msg['stack'] = e.stack;

      observable.notify(msg);
    }
  });

  return observable;
};

// static variable used accross instances
TestObservable.numErrors_ = 0;

//
// The tests
// ---------

var assert = ( typeof phantom === 'object' && require('chai').assert ) ||
             ( typeof process === 'object' && require('assert') ) ||
             console.assert;

test1 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

test2 = function() {
  assert.ok(true, 'this should not throw an error');
  assert.ok(false, 'this should throw an error');
};

// would be nice with something like   .json2tap()
(TestObservable.test(test1))
.merge(TestObservable.test(test2))
  .map(TestObservable.basicFormat)
  .map(console.log.bind(console))
