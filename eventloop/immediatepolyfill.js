immediate = function(fn) {
  var run = (typeof setImmediate === 'function' && setImmediate) || 
            (typeof process === 'object' && process.nextTick) || 
            (typeof setTimeout === 'function' && setTimeout) || 
            null;

  if (!run) return fn();
  if (run === setTimeout) {
    setTimeout(fn, 0);
    return;
  }
  return run(fn);
}

immediate(function() {
  console.log('Running immediate 1')
}, 10);

immediate(function() {
  console.log('Running immediate 2')
}, 11);
