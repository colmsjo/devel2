/* 

Playing around with functional combinators.

Links:

 * http://raganwald.com/2013/01/03/function_and_method_decorators.html
 * http://allong.es

*/

var assert = require('assert');

function compose (fn1, fn2) {
  return function compose_ (something) {
    return fn1(fn2(something));
  }
}

// make fn a function where arguments can be partially applied
// fn must take a array an argument. P(fn)([1,,3])([,2,undefined]) === fn([1,2,3])
// NOTE: the last element in an array must be specifically set to undefined
var P = function (fn) {
  function combine(a1,a2) {
    var res = [];
    for(var i=0; i<a1.length; i++)
      res.push( a1[i] !== undefined ? a1[i] : a2[i] );
    return res;
  }

  function undef(a) {
    for(var i=0; i<a.length; i++)
      a[i] = a[i] !== undefined ? a[i] : undefined;
    return a;
  }

  return function invoke(args) {
    args = undef(args);
    if (args.indexOf(undefined) === -1) {
      return fn.apply(null,args);
    } else {
      return function(args2) {
        if (!(args2 instanceof Array)) {
          args2 = Array.prototype.slice.call(arguments);
        }
        args2 = undef(args2);
        return invoke(combine(args,args2));
      };
    }
  }
}

J = P(JSON.stringify);
I = J([,null,4]);
console.log( I([{one:1},,undefined]) );
console.log( I({one:1}) );


function tot(a,b,c){return a+b+c};
fn = P(tot);

var U = undefined;

// last element in an array must specifically be defined as undefined
// can't do [1,2,], must do [1,2,U]

console.log('Running tests. No output means everything is ok');
assert(tot(1,2,3) === fn([1,2,3]), 'tot should equal P(tot) with full array');
assert(tot(1,2,3) === fn([1,2,U])([,,3]), 'one parital');
assert(tot(1,2,3) === fn([1,,U])([,2,])([,,3]), 'two paritals');


