// curry arbitary argument (not only the first)

// Uncurry `this` in `slice`.
var slice = Function.prototype.call.bind(Array.prototype.slice);

// self is not needed, undefined is allowed in apply (and bind)
function curry(fn, arg, index, self) {
    return function curried(/* ...args */) {
        var args = slice(arguments);
        args.splice(index, 0, arg);
        return fn.apply(self, args);
    };
}

function tag(name, value) {  
    return '<' + name + '>' + value + '</' + name + '>';
}
function otherTag(value, name) {  
    return '<' + name + '>' + value + '</' + name + '>';
}

var strong = curry(tag, "strong", 0);
console.log(strong("text"));

var otherStrong = curry(otherTag, "strong", 1);
console.log(otherStrong("text"));

