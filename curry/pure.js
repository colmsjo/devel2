// http://stackoverflow.com/questions/22127889/javascript-s-bind-vs-curry

function tag(name, value) {  
    return '<' + name + '>' + value + '</' + name + '>';
}

var strong = tag.bind(undefined, "strong");
console.log(strong("text"));


var add = function(a, b){ return a + b };
var add100 = add.bind(undefined, 100)

console.log(add100(1))

