
(function(self_, undefined) {

  var u        = require('underscore');
  var readable = require('stream').Readable;
  var writable = require('stream').Writable;
  var util     = require('util');

  var h        = require('./helpers.js');

  // set debugging
  h.debug = true;

  //
  // Leveldb readable stream
  // =======================
  //

  exports.LevelDBReadStream = function() {

    var self = this;

    // call Writable stream constructor
    readable.call(this);

    //
    // Privileged properties
    // ---------------------

    self._noReadChunks     = 0;

    // These are initialized in the init function
    self._key             = null;
    self._currentRevision = null;
    self.levelup          = null;
    self.leveldb          = null;
  };

  // inherit stream.Readable
  exports.LevelDBReadStream.prototype = Object.create(readable.prototype);

  // Initialize leveldb object
  exports.LevelDBReadStream.prototype.init = function(key, cb) {

    var self = this;
    h.log.log('LevelDB.init: key=' + key);

    // Open the LevelDB database
    self.levelup = require('level');
    self.leveldb = self.levelup('./mydb');

    // save for later use
    self._key               = key;

    // get the current revision and then run the callback
    h.getCurrentRev(self.leveldb, key, self, cb);
  };

  // create a leveldb stream and pipe it into a provided write stream
  exports.LevelDBReadStream.prototype.pipeReadStream = function(writeStream) {

    var self = this;
    h.log.log('LevelDB.pipeReadStream: key=' + self._key + ', rev=' + self._currentRevision);

    var _revision = h.pad(self._currentRevision, 9);

    // create stream that reads all chunks
    var _valueStream = self.leveldb.createReadStream({
      start:   self._key + '~' + _revision + '~000000000',
      end:     self._key + '~' + _revision + '~999999999',
      limit:   999999999,
      reverse: false,
      keys:    false,
      values:  true
    });

    // pipe the created stream into the provided write stream
    _valueStream.pipe(writeStream);
  };


  //
  // Leveldb writable stream
  // =======================
  //

  exports.LevelDBWriteStream = function() {

    var self = this;

    // call Writable stream constructor
    writable.call(this);

    //
    // Privileged properties
    // ---------------------

    self._noSavedChunks    = 0;

    // These are initialized in the init function
    self._key             = null;
    self._currentRevision = null;
    self.levelup          = null;
    self.leveldb          = null;

  };


  // inherit stream.Writeable
  //LevelDBWriteStream.prototype = Object.create(writable.prototype);
  util.inherits(exports.LevelDBWriteStream, writable);

  // Initialize leveldb object
  exports.LevelDBWriteStream.prototype.init = function(key, cb) {

    var self = this;
    h.log.log('LevelDB.init: key=' + key);

    // Open the LevelDB database
    self.levelup = require('level');
    self.leveldb = self.levelup('./mydb');

    // save for later use
    self._key               = key;

    // get the current revision and then run the callback
    h.getCurrentRev(self.leveldb, key, self,
      function(){ self._currentRevision++; cb();}
    );

  };

  // override the write function
  exports.LevelDBWriteStream.prototype._write = function (chunk, encoding, done) {

    var self = this;
    var _k = h.formatKey(self._key,
                             self._currentRevision,
                             ++self._noSavedChunks);

    self.leveldb.put(_k, chunk, function(err) {
      h.log.debug('WROTE: chunk ('+self._key+','+
                                      self._currentRevision+','+
                                      self._noSavedChunks+''+')');

      if (err) return h.log.log('LevelDB._write: error saving chunk!', err);

      // finished processing chunk
      done();

    });

  };

  // Finish up and close the stream
  exports.LevelDBWriteStream.prototype.close = function () {
    var self = this;

    // save the last chunk if provided
    // risk that close below is exec first? if(arguments.length > 0) this._write(arguments[0]);

    // close the leveldb properly
    self.leveldb.close();

    // call the super end fucntion
    //self.super_.end.apply(arguments);
  };

})(this);
