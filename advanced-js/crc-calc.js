    var makeCRCTable = function() {
      var c;
      var crcTable = [];
      for (var n = 0; n < 256; n++) {
        c = n;
        for (var k = 0; k < 8; k++) {
          c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
      }
      console.log("CREATED")
      return crcTable;
    }
    this.crcTable = makeCRCTable();


     var crc32_gp = function(str) {
      var crcTable = this.crcTable || (this.crcTable = makeCRCTable());
      var crc = 0 ^ (-1);
    
      for (var i = 0; i < str.length; i++) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
      }
    
      return (crc ^ (-1)) >>> 0;
    };


    var calcShortCRC = function(str, max) {
        return crc32_gp(str) % max;
    }


console.log('calculating: ' + calcShortCRC('asdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('bsdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('csdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('dsdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('esdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('fsdfghjkzxcv', 3) );
console.log('calculating: ' + calcShortCRC('fsdfghjkzxcw', 3) );
console.log('calculating: ' + calcShortCRC('fsdfghjkzxcx', 3) );
console.log('calculating: ' + calcShortCRC('fsdfghjkzxcy', 3) );

var i = 0;
while(i++ < 1e6) {
    calcShortCRC('asdfghjkzxcv', 3);
}
