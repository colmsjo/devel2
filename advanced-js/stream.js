


// Define custom stream class
// --------------------------

var Writable = require('stream').Writable;
var util     = require('util');


var myWritableStream = function() {
  // call stream.Writeable constructor
  Writable.call(this);
};


// inherit stream.Writeable
myWritableStream.prototype = Object.create(Writable.prototype);

// override the write function
myWritableStream.prototype._write = function (chunk, encoding, done) {
  console.log(chunk.toString());
  done();
};



// Main
// -----

console.log('piping stdin to stdout using my own writale stream...');

var myStream = new myWritableStream();
process.stdin.pipe(myStream);
