//
// Illustration of closures
//
// npm install async
//

async = require('async');

var a = function() {
  var _a;

  this.init = function() { console.log('test 1 init a:'+this._a); this._a = 1; console.log('test 1 init a:'+this._a); };
  this.print = function() { console.log('test 1 print a:'+this._a); };
}

var b = new a();
b.init();

// wait 1 second and run b.print
setTimeout(  b.print, 1000);

// wait 2 seconds and run b.print using a closure
setTimeout(  function() { b.print(); }, 2000);


// wait three seconds and run
setTimeout(
function() {
  var c = function() {
    var _a;

    this.init = function() { console.log('test 2 init a:'+this._a); this._a = 1; console.log('test 2 init a:'+this._a); };

    this.print = function() {
      async.series([
        function(cb) { console.log('test 2 async without bind print a:'+this._a); cb(null, 'one'); },
        function(cb) { console.log('test 2 async with bind print a:'+this._a); cb(null, 'two'); }.bind(this)
      ]);

    };

  }

  var d = new c();
  d.init();
  d.print();
}
, 3000);
