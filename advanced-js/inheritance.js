// SIMPLE SOLUTION: JUST USE node built in util.inherts!!
// See stream.js for an example

// Always put properties in the constructor and methods on the prototype
// This way are properties  not shared among instances but methods are


/* Good apparoach according to some article I googled */

// I'd like to move the Animal.prototype.move  inside the constructor but
// this don't work
function Animal(name) {
  this.name = name;
}

Animal.prototype.move = function(meters) {
  console.log(this.name+" moved "+meters+"m.");
};

function Snake() {
  Animal.apply(this, Array.prototype.slice.call(arguments));
}

Snake.prototype = new Animal();
Snake.prototype.move = function() {
  console.log("Slithering...");
  Animal.prototype.move.call(this, 5);
};

var sam = new Snake("Sammy the Python");
sam.move();




// My old  not so good approach, did not works for streams
// -------------------------------------------------------

// on_data and on_end needs to bind to request and response
// a function is the returned that can be used on on('data') and on('end')

var BackendAdapter = function() {
    this.on_data = function(request, response) {
      console.log('BackendAdapter is abstract. on_data() needs to be implemented');
    };

    this.on_end = function(request, response) {
      console.log('BackendAdapter is abstract. on_end() needs to be implemented');
    };
};


var mySqlAdapter = function() {

  this.prototype = BackendAdapter;
  this.prototype(this);

  this.on_end = function(req, res) {
    console.log('function implemented');
  };

};


var s = new mySqlAdapter();
s.on_data('a', 'b');
s.on_end('a', 'b');
