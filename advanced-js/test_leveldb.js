var rs = require('./leveldb_streams.js').LevelDBReadStream;
var ws = require('./leveldb_streams.js').LevelDBWriteStream;
var h  = require('./helpers.js');


// Main
// ====

console.log('piping stdin to leveldb and stdout using my own writale stream...');

// Create stream and then pipe stdin input it
var leveldbWrite = new ws();
leveldbWrite.init('stdin', function() {
  process.stdin.pipe(leveldbWrite);
});


// Read the leveldb after X seconds and print to stdout
setTimeout(function() {
  leveldbWrite.close();

  h.log.debug('VALUES of the levelddb stdin stream:');

  var leveldbRead = new rs();
  leveldbRead.init('stdin', function() {
    leveldbRead.pipeReadStream(process.stdout);
  });
}.bind(this), 10000);
