var events = require('events');

// Class emitting events
// ----------------------

var Eventer = function () {
  events.EventEmitter.call(this);
};

Eventer.prototype = Object.create(events.EventEmitter.prototype);

Eventer.prototype.kapow = function() {
  var data = "BATMAN";
  this.emit('blamo', data);
};

Eventer.prototype.bam = function() {
  this.emit("boom");
};


// Class listening to events
// ------------------------

var Listener = function() {
};

Listener.prototype.blamoHandler = function(data) {
  console.log("** blamo event handled");
  console.log(data);
};

Listener.prototype.boomHandler = function(data) {
  console.log("** boom event handled");
};

// The thing that drives the two.
// ------------------------------

var eventer = new Eventer();
var listener = new Listener();
eventer.on('blamo', listener.blamoHandler);
eventer.on('boom', listener.boomHandler);

eventer.kapow();
eventer.bam();
