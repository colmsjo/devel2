JavaScript inheritance
======================

Would be nice to crate an abstract base class for adapters that then is implemetned
with MySQL and leveldb implemetations.

 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Inheritance_and_the_prototype_chain
 * http://javascriptweblog.wordpress.com/2011/05/31/a-fresh-look-at-javascript-mixins/
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/call


SKILLNAD MELLAN FUNCTION OCH RENA OBJEKT

var f = function() { this.A = function() {console.log('HEJ');} };
> f.A;
undefined
> new f().A();
HEJ
undefined

var f = { A : function() {console.log('HEJ');}};
> f.A();
HEJ
undefined


Partial binding
---------------

function f(req, res){ return function(chunk) {console.log(chunk)}; };
> f(1,2)('HEJ');
HEJ
undefined


base class and inheritance - BAD IDEA!
-------------------------------------


// on_data and on_end needs to bind to request and response
// a function is the returned that can be used on on('data') and on('end')

var BackendAdapter = function() {
    this.on_data = function(request, response) {
        return function(chunk) {
            throw new Error('BackendAdapter is abstract. on_data() needs to be implemented');
        };
    };

    this.on_end = function(request, response) {
        return function() {
            throw new Error('BackendAdapter is abstract. on_end() needs to be implemented');
        };
    };
};


var mySqlAdapter = function() {
  this.prototype = BackendAdapter;
  this.prototype(this);

  this.on_data = function(chunk, request, response) {
      ...
  };

};


Better approach
---------------

Define properties in the constructor and methods in the prototype

function Animal(name) {
  this.name = name;
};
Animal.prototype.move = function(meters) {
  console.log(this.name+" moved "+meters+"m.");
};

function Snake() {
  Animal.apply(this, Array.prototype.slice.call(arguments));
};
Snake.prototype = new Animal();
Snake.prototype.move = function() {
  console.log("Slithering...");
  Animal.prototype.move.call(this, 5);
};

var sam = new Snake("Sammy the Python");
sam.move();
