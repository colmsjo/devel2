var http = require("http");


var start_server = function() {

  var server = http.createServer(function(request, response) {

    console.log("Processing request: " +
      JSON.stringify(request.method) + " - " +
      JSON.stringify(request.url) + " - " +
      JSON.stringify(request.headers));


    // save input from POST and PUT here
    var data = '',
      counter = 0;

    request.on('data', function(chunk) {

      if (request.method == 'POST') {
        console.log('http server recieved post: ' + chunk);
        data += chunk;
      }

    });

    // request closed, process it
    request.on('end', function() {
      try {
        // parse data and prepare insert for POST requests
        if (request.method == 'POST') {

          console.log('request end event. Will close response now.');

          response.writeHead(200, {
            "Content-Type": "application/json"
          });
          response.write('{status: ok, data:'+data+'}');
          response.end();
        }
      } catch (e) {
        writeError(response, e);
      }

    });
  });

  server.listen(8080);
  console.log("Server is listening on port 8080");

};


var run_client = function() {
  // path and method is set in each test
  var options = {
    hostname: 'localhost',
    port: 8080,
    method: 'POST',
    path: '/image1',
    headers: {
      user: 'wp',
      password: 'wp',
    }
  };

  var data = '';

  var req = http.request(options, function(res) {
    console.log('in http.request');
    res.setEncoding('utf8');

    res.on('data', function(chunk) {
      console.log('in http.request on data: ' + chunk);
      data += chunk;
    });

    res.on('end', function(chunk) {
      console.log('end of http.request: ' + data);
    });

  });

  req.on('error', function(e) {
    console.log('problem with request: ' + e.message);
  });

  req.write('Writing some data...');
  req.write('Writing some more data...');
  req.end();

};

start_server();
run_client();
