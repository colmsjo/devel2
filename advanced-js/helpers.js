//
// Misc helpers fucntions
// =====================


(function(self_, undefined) {

  var h = self_.helpers || {};
  var u = require('underscore');

  // change to false to stop logging
  h.debug     = false;
  h.info      = true;
  h.noLogging = false;

  h.log = {

    debug: function(text) {
      if (h.debug && !h.noLogging) console.log('DEBUG: ' + text);
    },

    info: function(text) {
      if (h.info && !h.noLogging) console.log('INFO: ' + text);
    },

    log: function(text) {
      if (!h.noLogging) console.log(text);
    }
  };

  // converts a number to a string and pads it with zeros: pad(5,1) -> 00001
  // a - the number to convert
  // b - number of resulting characters
  h.pad = function(a, b) {
    return (1e15 + a + "").slice(-b);
  };


  //
  // Leveldb Helpers
  // ----------------

  // Store data/blobs in chunks in the database. Keys have the following form:
  // key~rev#~chunk#
  // rev# and chunk# are 9 digits key~000000001~000000001
  //


  // Read keys into an array and process with callback
  // maximum 999.999.999 revisions and 999.999.999 chunks
  h.readKeys = function(leveldb, keyPrefix, cb) {

    var _keyStream = leveldb.createReadStream({
      start:   keyPrefix + '~000000000',
      end:     keyPrefix + '~999999999',
      limit:   999999999,
      reverse: false,
      keys:    true,
      values:  false
    });

    var _keys = [];

    _keyStream.on('data', function(data) {
      _keys.push(data);
    });

    _keyStream.on('error', function(err) {
      log.log('Error reading leveldb stream: ' + err);
    });

    _keyStream.on('close', function() {
      h.log.debug('_readKeys: '+JSON.stringify(_keys));
      cb(_keys);
    });
  };

  // Read all chunks for file and process chunk by chunk
  // maximum 999.999.999 revisions and 999.999.999 chunks
  h.readValue = function(leveldb, keyPrefix, revision, cbData, cbEnd) {

    var _revision = pad(revision, 9);

    var _keyStream = leveldb.createReadStream({
      start:   keyPrefix + '~' + _revision + '~000000000',
      end:     keyPrefix + '~' + _revision + '~999999999',
      limit:   999999999,
      reverse: false,
      keys:    false,
      values:  true
    });

    _keyStream.on('data', function(data) {
      cbData(data);
    });

    _keyStream.on('error', function(err) {
      h.log.log('Error reading leveldb stream: ' + err);
    });

    _keyStream.on('close', function() {
      cbEnd();
    });
  };

  // Get the last revison of a key and run callback
  // -1 is used if the file does not exist
  h.getCurrentRev = function(leveldb, keyPrefix, revObj, cb) {

    var currentRevision = -1;

    h.readKeys(leveldb, keyPrefix, function(keys) {

      if (keys.length > 0) {
        var _revs = u.map(
          keys,
          function(k) {
            return k.slice(keyPrefix.length + 1, keyPrefix.length + 1 + 9);
          }
        );

        currentRevision = parseInt(u.max(_revs, function(r) { return parseInt(r); }));
      }

      h.log.debug('LevelDB.getCurrentRev: keyPrefix=' + keyPrefix + ', rev= ' +
        currentRevision);

      // Save revision and run callback
      revObj._currentRevision = currentRevision;
      cb(currentRevision);
    });
  };

  // format a key, revision and chunk: key~000000001~000000000
  h.formatKey = function(k, revNum, chunkNum) {
    return k + '~' + h.pad(revNum, 9) + '~' + h.pad(chunkNum, 9);
  };



  // Exports
  // =======

  module.exports = h;

})(this);
