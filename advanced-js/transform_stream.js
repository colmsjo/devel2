//
// Simple example of how transforms can be used
//
// process.stdin.pipe(liner) - pipe stdin to liner
// line = liner.read - pulls data from stdin
// console.log(line) - prints the same data to stdout
//
//
//


var stream = require('stream');
var liner = new stream.Transform({
  objectMode: true
});

// the input stream will use this function when writing data
liner._transform = function(chunk, encoding, done) {
  var data = chunk.toString();
  if (this._lastLineData) data = this._lastLineData + data;

  var lines = data.split('\n');
  // save the part after the last new line
  this._lastLineData = lines.splice(lines.length - 1, 1)[0];

  // push is inherited from Readble
  lines.forEach(this.push.bind(this));
  done();
};


// push the last remaining data (if any)
liner._flush = function(done) {
  if (this._lastLineData) this.push(this._lastLineData);
  this._lastLineData = null;
  done();
};


console.log('Piping stdin to my transform. Enter some line delimited input. Use ctrl-c to break out.');

process.stdin.pipe(liner);
liner.on('readable', function () {
  var line;
  while (line = liner.read()) {
    // do something with line
    console.log('The transformed input:'+line);
  }
});
