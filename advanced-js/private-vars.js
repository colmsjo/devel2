var f = function() {
  var _a;

  this.init1 = function() { this._a = 1; };
  this.init2 = function() { _a = 2; };
  this.print = function() { console.log('this._a:'+this._a); console.log('_a:'+_a); }
};

var g = new f();
g.init1();
console.log('print after init1');
console.log('access g._a:'+g._a);
g.print();
g.init2();
console.log('print after init2');
g.print();


// class with private and privileged functions
var f2 = function(text) {

    var _message = text;
    this._message = text;

    // this function is not accessable to public function
    var privateFunc = function() { console.log(_message); };

    // This function is accessable
    this.privilegedFunc = function() { privateFunc(); };
};

f2.prototype.publicFunc1 = function () {
  privateFunc();
};

f2.prototype.publicFunc2 = function () {
  this.privilegedFunc();
};

var i1 = new f2('Message to print');
i1.publicFunc1(); // This don't work
i1.publicFunc2(); // This works
