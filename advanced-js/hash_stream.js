var fs = require('fs');

var calcHash = function(readStream, alg, enc, cb) {
  var crypto = require('crypto');
  var hash = crypto.createHash(alg);

  hash.setEncoding(enc);

  fd.on('end', function() {
    hash.end();
    cb(hash.read());
  });

  // read all file and pipe it (write it) to the hash object
  readStream.pipe(hash);
};

var fd = fs.createReadStream('./projektledning_w767.png');

calcHash(fd, 'sha1', 'hex', function(hash) {
  console.log('The calculated hash: ' + hash);
});
