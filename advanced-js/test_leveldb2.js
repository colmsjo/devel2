var rs = require('./leveldb_streams.js').LevelDBReadStream;
var ws = require('./leveldb_streams.js').LevelDBWriteStream;
var h = require('./helpers.js');


// Main
// ====

console.log('piping file to leveldb and stdout using my own writale stream...');

fs = require('fs');
var readStream = fs.createReadStream('./projektledning_w767.png');
var leveldbWrite = new ws();

readStream.on('open', function() {
  h.log.debug('in readStream on open');

  // Create stream and then pipe stdin input it
  leveldbWrite.init('projektledning_w767', function() {
    readStream.pipe(leveldbWrite);
  });

});

readStream.on('end', function() {
    h.log.debug('end event in readStream');
    leveldbWrite.close();
});


// Read the leveldb after X seconds and print to stdout
setTimeout(function() {
  h.log.debug('VALUES of the levelddb stdin stream:');

  var leveldbRead = new rs();
  var writeFS = fs.createWriteStream('./output', {
    flags: 'w',
    encoding: null,
    mode: 0666
  });

  leveldbRead.init('projektledning_w767', function() {
    leveldbRead.pipeReadStream(writeFS);
  });
}.bind(this), 2000);
