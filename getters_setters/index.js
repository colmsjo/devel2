var o = {}
Object.defineProperty(o, 'b', {
  get: function () {
    console.log('getting b_:', this.b_);
    return this.b_;
  },
  set: function (b) {
    console.log('setting b_:', b);
    this.b_ = b;
  },
  enumerable: true,
  configurable: true
});

o.b = 12;
console.log(o.b);

var createObj = function (props) {
  var o = Object.create({});
  props.forEach(function(prop) {
    Object.defineProperty(o, prop, {
      get: function () {
        console.log('getting ', prop, ':', this[prop+'_']);
        return this[prop+'_'];
      },
      set: function (val) {
        console.log('setting ', prop, ':', val);
        this[prop+'_'] = val;
      },
      enumerable: true,
      configurable: true
    });
  });

  return o;
}

var o1 = createObj(['a', 'b', 'c']);
o1.a = o1.b = o1.c = 42;
console.log(o1.c);
