var foo = require('./foo');
var gamma = require('gamma');

// foo module
var n = gamma(foo(5) * 3);
var txt = document.createTextNode(n);

document.body.appendChild(txt);
