From: http://www.100percentjs.com/just-like-grunt-gulp-browserify-now/

1. `npm install -g browserify`

1. `npm install gamma`

1. Do `gulp`. This is equivalent with `browserify -d main.js -o bundle.js`.
The `-d` flag generates a source-map with makes it possible to debug in the
source files (instead of the compiled bundle).

1. Open `index.html` in a browser


NOTE
---

The `foo2` module does not work to compile with browserify. It does work in
both NodeJS and browsers using traditional includes though.
