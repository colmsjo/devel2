var f = function(n) { return n * 11; }


//
// Manage modules
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify


// avoid errors in the browsers
var inBrowser = (typeof module === 'undefined');
if (inBrowser) window.module = { exports: {} };

// The name of the module becomes the filename automatically in browserify
// (here foo)
module.exports = f;

// export the module foo without browserify
if (inBrowser) window['foo'] = module.exports;

// Make require just return the Object with the required name in the browser
if (inBrowser) {
  require = function(m) {
    if (window[m] === 'undefined')
      throw 'Module: ' + m + ' not loaded! This needs to be done suing th script tag when not using browserify.';

    return window[m];
  }
}
