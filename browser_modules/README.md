
Links:
 * http://stackoverflow.com/questions/16521471/relation-between-commonjs-amd-and-requirejs

 There are two main specifications for JavaScript modules:

  * CommonJS - used by Node and browserify
  * AMD - used by RequireJS. Mainly used in browsers but there are insstructions
    for running in node also

The main difference is that AMD  (Asynchronous Module Definition) is asynchronous.


RequireJS
---------


 * https://github.com/jrburke/requirejs



How browsers load scripts
-------------------------


 * http://www.html5rocks.com/en/tutorials/speed/script-loading/


This will block the rendering of the DOM until first 1.js has been loaded
and then 2.js (sequencially):

  <script src="//other-domain.com/1.js"></script>
  <script src="2.js"></script>

That's why scripts are put at the end of the body.



backet.js - Cache script in localstorage
----------------------------------------

Links:

 * http://addyosmani.github.io/basket.js/
 * https://github.com/addyosmani/basket.js/blob/gh-pages/lib/basket.js




Promise implementation - https://github.com/tildeio/rsvp.js/


    basket.require({ url: 'jquery.js' });

    basket.require(
        { url: 'require.js' },
        { url: 'require.config.js', skipCache: true },
        { url: 'libs.js' }
    );


    basket
        .require({ url: 'missing.js' })
        .then(function () {
            // Success
        }, function (error) {
            // There was an error fetching the script
            console.log(error);
        });



    // expire in 2 hours
    basket.require({ url: 'jquery.min.js', expire: 2 })


    // download without executing
    basket.require({ url: 'jquery.min.js', execute: false });


    // fetch and cache the file
    basket.require({ url: 'jquery.min.js' });
    // fetch and cache again
    basket.require({ url: 'jquery.min.js', unique: 123 });


    var req
    var ttl;

    basket.require({ url: 'jquery.min.js', key: 'jquery' });
    req = basket.get('jquery');
    // know the lifetime
    ttl = req.expire - req.stamp;

    // remove key from localStorage
    basket
        .remove('jquery.js')
        .remove('modernizr');

    // remove all
    basket.clear();
    // remove expired
    basket.clear(true);
