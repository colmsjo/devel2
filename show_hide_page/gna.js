// empty constructor
var Gna = function() {

};


// This will crate a script tag with the code in `data`
Gna.prototype.loadScript = function(data, id) {
  id = (id !== undefined && id !== null) ? '$$' + id + '$$' : null;

  // check if script is loaded
  if(id !== undefined && document.getElementById(id) !== null) {
    return;
  }

  var head = document.head || document.getElementsByTagName('head')[0];
  var script = document.createElement('script');
  script.defer = true;
  script.async = false;
  script.text = data;
  if (id !== null) script.id = id;
  head.appendChild(script);
};


// remove all current stylesheets and load a new. Call without argument
// to just delete all current stylesheets
Gna.prototype.loadStylesheet = function(cssData) {
  var head = document.head || document.getElementsByTagName('head')[0];

  // remove the current stylesheets
  while (document.getElementsByTagName('style').length > 0) {
    head.removeChild(document.getElementsByTagName('style')[0]);
  }

  // create the new stylesheet
  if (cssData !== undefined && cssData !== null) {

    var style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet) {
      style.styleSheet.cssText = cssData;
    } else {
      style.appendChild(document.createTextNode(cssData));
    }

    head.appendChild(style);
  }
};

Gna.prototype.loadTemplate = function(htmlId, scriptId, cssId) {
  var self = this;

  if (htmlId === undefined || htmlId === null)
    throw 'Error in Gna.load! Mandatory attribute htmlId missing.'

  // Initialize some attributes
  self.body = (self.body === undefined) ? {} : self.body;
  self.script = (self.script === undefined) ? {} : self.script;
  self.css = (self.css === undefined) ? {} : self.css;

  var html = document.getElementById(htmlId).innerHTML;

  self.body[htmlId] = document.createElement('body');
  self.body[htmlId].innerHTML = html;

  if (scriptId !== undefined && scriptId !== null) {
    self.script[htmlId] = document.getElementById(scriptId).innerHTML;
  }

  if (cssId !== undefined && cssId !== null) {
    self.css[htmlId] = document.getElementById(cssId).innerHTML;
  }

};

Gna.prototype.showTemplate = function(htmlId) {
  var self = this;

  if (htmlId === undefined || htmlId === null)
    throw 'Error in Gna.show! Mandatory attribute htmlId missing.'

  document.body = self.body[htmlId];

  if (self.script[htmlId] !== undefined) {
    self.loadScript(self.script[htmlId], htmlId);
  }

  // switch stylesheet (or just delete the existing)
  self.loadStylesheet(self.css[htmlId]);

};


//
// ---------------------------------------------------------------------------
//

Gna.prototype.load = function(pageDef) {
  var self = this;

  // check mandatory input
  if (pageDef.id === undefined ||
    pageDef.html === undefined ||
    pageDef.eventHandler === undefined) throw "Invalid page definition";

  pageDef.element = document.createElement('body');
  pageDef.element.id = pageDef.id;
  pageDef.element.innerHTML = pageDef.html;

  // create shortcut
  if (pageDef.shortcut) {
    Mousetrap.bind(pageDef.shortcut, function() {
      self.show(pageDef);
    });
  }
};

Gna.prototype.show = function(pageDef) {

  var head = document.head || document.getElementsByTagName('head')[0];

  // create the page if it does not exist
  if (pageDef.element === undefined)
    createPage(pageDef);

  // set the new event handler
  pageEventHandler = pageDef.eventHandler;

  // add the new page
  document.body = pageDef.element;

  // set the title to the page description
  if (pageDef.description !== undefined)
    document.title = pageDef.description

  // add shortcut to description
  if (pageDef.shortcut !== undefined)
    document.title += ' (' + pageDef.shortcut + ')';

  // remove the current stylesheets
  while (document.getElementsByTagName('style').length > 0) {
    head.removeChild(document.getElementsByTagName('style')[0]);
  }

  // create the new stylesheet
  if (pageDef.style !== undefined &&
    pageDef.style !== null) {

    var css = pageDef.style,
      style = document.createElement('style');

    style.type = 'text/css';
    if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);
  }

}

var handleEvent = function(evt) {
  if(typeof pageEventHandler !== 'undefined') pageEventHandler(evt);
};

// supported events
window.addEventListener('input', handleEvent, false);
window.addEventListener('focusout', handleEvent, false);
window.addEventListener('click', handleEvent, false);


//
// Manage modules
// ---------------------------------------------------------------------------
// This is a small hack to make it possbile to use the module both with and
// without browserify


// avoid errors in the browsers
var inBrowser = (typeof module === 'undefined');
if (inBrowser) window.module = {
  exports: {}
};

// The name of the module becomes the filename automatically in browserify
// (here foo)
module.exports = Gna;

// export the module foo without browserify
if (inBrowser) window['Gna'] = module.exports;

// Make require just return the Object with the required name in the browser
if (inBrowser) {
  require = function(m) {
    if (window[m] === 'undefined')
      throw 'Module: ' + m + ' not loaded! This needs to be done using th script tag when not using browserify.';

    return window[m];
  }
}
