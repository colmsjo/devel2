mymod = function(options) {
  // make sure that new is used
  if (!(this instanceof mymod)) return new mymod(options);

  this.options_ = options;
}

mymod.prototype.getOptions = function() {
  return this.options_;
};

module.exports = mymod;
