//
// Fluent functions return this rather the the value updated etc.
//
// Allows code like this:
// 
// var cake = new Cake()
//   .setFlavour('chocolate')
//   .setLayers(3)
//   .bake();

function fluent (methodBody) {
  return function () {
    methodBody.apply(this,arguments);
    return this
  }
}

// A decorator for this isn't really necessary. Just return this at the end
// if the method instead??
