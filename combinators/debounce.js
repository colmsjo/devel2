//
// Some events fire often and degrades performance. debounce/throttle can be used to
// prevent this.



// THIS IMPLEMENTATION DON'T WORK!! Every call to the debounced function will cancel out 
// the previous call.

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce0(func, wait, immediate) {
	var timeout;
	return function() {
console.log('fn called');
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

// This implementation actualy wroks
function throttle(fn, threshhold, scope) {
  threshhold || (threshhold = 250);
  var last,
      deferTimer;
  return function () {
    var context = scope || this;

    var now = +new Date,
        args = arguments;
    if (last && now < last + threshhold) {
      // hold on to it
      clearTimeout(deferTimer);
      deferTimer = setTimeout(function () {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

var eventRate = 1000;
var fireRate = 3000; 

var last, now;
last = now = new Date();

var fn = throttle(function() {
  last = now;
  now = new Date()

  console.log('time elasped: ', last - now);

}, fireRate);

setInterval(fn, eventRate);




