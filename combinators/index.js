/*

Playing around with functional combinators.

Links:
 * http://raganwald.com/2013/01/01/this.html
 * http://raganwald.com/2013/01/03/function_and_method_decorators.html
 * http://allong.es

*/

var assert = require('assert');

function L (fn) {
  return function () {
    var result = fn.apply(this, arguments);
    console.log(result);
    return result;
  }
};

function O (func) {
  var ran = false, memo;
  return function() {
    if (ran) return memo;
    ran = true;
    memo = func.apply(this, arguments);
    func = null;
    return memo;
  };
};

function C (fn1, fn2) {
  return function compose_ (something) {
    return fn1.call(this, fn2.call(this, something));
  }
}

// see ../curry/mycurry.js for a nice partial generator

// make fn a function where arguments can be partially applied
// fn must take a array an argument. P(fn)([1,,3])([,2,undefined]) === fn([1,2,3])
// NOTE: the last element in an array must be specifically set to undefined
var P = function (fn) {
  function combine(a1,a2) {
    var res = [];
    for(var i=0; i<a1.length; i++)
      res.push( a1[i] !== undefined ? a1[i] : a2[i] );
    return res;
  }

  function undef(a) {
    for(var i=0; i<a.length; i++)
      a[i] = a[i] !== undefined ? a[i] : undefined;
    return a;
  }

  return function invoke(args) {
    args = undef(args);
    if (args.indexOf(undefined) === -1) {
      return fn.apply(null,args);
    } else {
      return function(args2) {
        if (!(args2 instanceof Array)) {
          args2 = Array.prototype.slice.call(arguments);
        }
        args2 = undef(args2);
        return invoke(combine(args,args2));
      };
    }
  }
}



var pretty = C(console.log, P(JSON.stringify)([,null,4]));
pretty({one: 1, two: 'two', three: 3.14});
