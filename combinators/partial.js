var H = {};

// Log result
H.L = function(fn) {
  return function() {
    var result = fn.apply(this, arguments);
    console.log(result);
    return result;
  }
};

// Run function once (from underscore)
H.O = function(func) {
  var ran = false,
    memo;
  return function() {
    if (ran) return memo;
    ran = true;
    memo = func.apply(this, arguments);
    func = null;
    return memo;
  };
};

// Compose two functions
H.C = function(fn1, fn2) {
  return function compose_(something) {
    return fn1.call(this, fn2.call(this, something));
  }
};

// Partial generator
// make fn a function where arguments can be partially applied
// P(fn)([1,,3])([,2,undefined]) === fn([1,2,3])
// It is not necessary to use an array as input when the first argument is left
// unbound. Here is an example:
//    var pretty = H.C(console.log, H.P(JSON.stringify)([,null,4]));
//    pretty({one: 1, two: 'two', three: 3.14});
// NOTE: the last element in an array must be specifically set to undefined
H.P = function(fn) {
  function combine(a1, a2) {
    var res = [];
    for (var i = 0; i < a1.length; i++)
      res.push(a1[i] !== undefined ? a1[i] : a2[i]);
    return res;
  }

  function undef(a) {
    for (var i = 0; i < a.length; i++)
      a[i] = a[i] !== undefined ? a[i] : undefined;
    return a;
  }

  return function invoke(args) {
    args = undef(args);
    if (args.indexOf(undefined) === -1) {
      return fn.apply(null, args);
    } else {
      return function(args2) {
        if (!(args2 instanceof Array)) {
          args2 = Array.prototype.slice.call(arguments);
        }
        args2 = undef(args2);
        return invoke(combine(args, args2));
      };
    }
  }
};

H.U = function(idx, length) {
  return function(v) {
    a = new Array(length);
    a[idx] = v;
    return a;
  }
};

// same as P(fn)([v1,undefined,...,vn])([undefined,val,...,undefined])
H.P0 = function(fn, idx) {
  return H.C(H.P(fn), H.U(idx, fn.length));
}

exports = H;

function add(a,b,c) {return a+b+c};

console.log(H.U(1,3)(2));
console.log( H.P(add)([1,undefined,3]) );

console.log( H.C( H.P(add)([1,undefined,3]), 
                  H.U(1,3) )(2) 
           );


//console.log( H.P0( add
