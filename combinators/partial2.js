var curry = require('curry');
var fs = require('fs');

//
// Swap function where the number of arguments are explicit
//

function swap_(fn, i1, i2) {
  var args = Array.prototype.slice.call(arguments);
  args.splice(0,3)
  var t = args[i1];
  args[i1] = args[i2];
  args[i2] = t;
  return fn.apply(null, args);
}

function swap4(fn, i1, i2) {
  return function(a, b, c, d) {
    return swap_(fn, i1, i2, a, b, c, d);
  }
}

var write = curry(fs.writeFile);
write('outfile.txt')('string to write')('utf8')(null);
console.log('check outfile.txt');

var write2 = swap4(fs.writeFile, 1, 3);
write2('outfile2.txt', null, 'utf8', 'string 2 to write');
console.log('check outfile2.txt');

var write3 = curry(write2);
write3('outfile3.txt')(null)('utf8')('string 3 to write');
console.log('check outfile3.txt');

//
// A better solution is to use curry.to
//

function swap(fn, i1, i2) {
  return function() {
    var args = Array.prototype.slice.call(arguments);
    var t = args[i1];
    args[i1] = args[i2];
    args[i2] = t;
    return fn.apply(null, args);
  }
}

var write4 = curry.to(4, swap(fs.writeFile, 1, 3) );
write4('outfile4.txt')(null)('utf8')('string 4 to write');
console.log('check outfile4.txt');
