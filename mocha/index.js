// npm install mocha
// ./node_modules/.bin/mocha index.js 

var assert = require("assert");

describe('Array', function(){
  describe('#indexOf()', function(){
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    });
  });

  describe('#indexOf()', function(){
    it('should return -1 when the value is not present (this test will fail)', function(){
      assert.equal(1, [1,2,3].indexOf(5));
      assert.equal(1, [1,2,3].indexOf(0));
    });
  });

})
