#!/bin/bash
#
#
# VBoxManage list ostypes

VM='smartos'

# Remove old VM if it is there
VBoxManage unregistervm $VM || true
rm $VM.vdi
rm -rf $VM

VBoxManage createhd --filename $VM.vdi --size 32768
VBoxManage createvm --name $VM --ostype "Solaris11_64" --register

# Sata controller with hard drive
VBoxManage storagectl $VM --name "SATA Controller" --add sata --controller IntelAHCI
VBoxManage storageattach $VM --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium $VM.vdi

# Ide controller with CD
VBoxManage storagectl $VM --name "IDE Controller" --add ide
VBoxManage storageattach $VM --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium "/Users/jonas/VirtualBox VMs/smartos-latest.iso"

# Misc settings
VBoxManage modifyvm $VM --ioapic on
VBoxManage modifyvm $VM --boot1 dvd --boot2 disk --boot3 none --boot4 none
VBoxManage modifyvm $VM --memory 1024 --vram 128
VBoxManage modifyvm $VM --nic1 bridged --bridgeadapter1 en0 --nictype1 82540EM --nicpromisc1 allow-all
VBoxManage modifyvm $VM --cpus 3

# Start the VM
echo "Follow the instructions in the VirtualBox windows to setup the VM"
VBoxManage --startvm $VM 
