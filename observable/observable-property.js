var Observable = function () {
  this.observers_ = [];
};

Observable.prototype.register = function (observer) {
  this.observers_.push(observer);
};

Observable.prototype.notify = function (evt) {
  this.observers_.forEach(function (o) {
    o.notify(evt)
  });
};

Observable.create = function (props) {
  var o = new Observable();
  props.forEach(function (prop) {
    Object.defineProperty(o, prop, {
      get: function () {
        console.log('getting ', prop, ':', this[prop + '_']);
        return this[prop + '_'];
      },
      set: function (val) {
        console.log('setting ', prop, ':', val);
        this[prop + '_'] = val;
        this.notify({
          target: this,
          property: prop,
          value: this[prop + '_']
        });
      },
      enumerable: true,
      configurable: true
    });
  });

  // used to avoid infinite loop in two-way binding
  o.silentSet = function(prop, val) {
    this[prop + '_'] = val;
  };

  return o;
};

var o1 = Observable.create(['a', 'b', 'c']);

o1.register({
  notify: function (evt) {
    console.log('observer notified, property: ', evt.property,
                ', new value: ', evt.value,
                ', using getter: ', evt.target[evt.property]);
  }
});

o1.register({
  notify: function (evt) {
    console.log('observer2 notified, property: ', evt.property,
                ', new value: ', evt.value,
                ', using getter: ', evt.target[evt.property]);
  }
});

o1.a = o1.b = o1.c = 42;
console.log(o1.c);

o1.silentSet('a',24);
console.log(o1.a);
